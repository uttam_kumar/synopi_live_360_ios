//
//  Counter.swift
//  SynopiLive360
//
//  Created by Syncrhonous on 23/5/19.
//  Copyright © 2019 Syncrhonous. All rights reserved.
//

//stream time counter
import Foundation

class StreamTimeCounter: NSObject {
    var timeCounter: UInt32 = 0
    func setData(timeCounter: UInt32){
        self.timeCounter = timeCounter
    }
    
    open func counting() -> String{
        let seconds = timeCounter % 60
        var minutes = timeCounter / 60
        let hours = minutes / 60
        minutes = minutes % 60
        
        var sec: String = ""
        var min: String = ""
        var hour: String = ""
        if seconds < 10 {
            sec = "0" + String(seconds)
        }else{
            sec = String(seconds)
        }
        if minutes < 10 {
            min = "0" + String(minutes)
        }else{
            min = String(minutes)
        }
        hour = "0" + String(hours)
        
        return "\(hour):\(min):\(sec)"
    }
}
