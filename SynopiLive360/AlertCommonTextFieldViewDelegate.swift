//
//  AlertCommonTextFieldViewDelegate.swift
//  SynopiLive360
//
//  Created by Syncrhonous on 4/4/19.
//  Copyright © 2019 Syncrhonous. All rights reserved.
//

protocol AlertCommonTextFieldViewDelegate: class {
    func okButtonTapped(selectedOption: String, textFieldValue: String)
    func cancelButtonTapped()
}

