//
//  StatusViewViewController.swift
//  SynopiLive360
//
//  Created by Syncrhonous on 25/5/19.
//  Copyright © 2019 Syncrhonous. All rights reserved.
//



//it comes after successfully or unseccessfully retrieves stream url and stream key
import UIKit
import Foundation
import TTGSnackbar

class StatusViewViewController: UIViewController {
    let reachability = Reachability()!
    let snackbar = TTGSnackbar(message: "TTGSnackbar !", duration: .forever)
    var isInternetConnected = true
    let viewHiderOrEnabler: ViewHiderOrEnablerStyler = ViewHiderOrEnablerStyler()
    let defaults = UserDefaults.standard
    @IBOutlet var statusView: UIView!
    @IBOutlet weak var statusTextLabel: UILabel!
    @IBOutlet weak var editServiceIdBtn: UIButton!
    @IBOutlet weak var editPhoneBtn: UIButton!
    @IBOutlet weak var continueBtn: UIButton!
    var isSuccessUi: Bool = false
    var phone: String = ""
    var service_id: String = ""
    var streamUrl:String = "empty"
    var streamKey:String = "empty"
    
    override func viewDidLoad() {
        super.viewDidLoad()
        //self.removeSpinner()
        let value = UIInterfaceOrientation.portrait.rawValue
        UIDevice.current.setValue(value, forKey: "orientation")
        self.defaults.set("StatusViewViewController", forKey: "currentViewController")
        
        //print("stskhtkehy")
        DispatchQueue.main.async {
            self.streamUrl = self.defaults.string(forKey: "publishUrl") ?? self.streamUrl
            self.streamKey = self.defaults.string(forKey: "publishKey") ?? self.streamKey
            
            if self.streamKey == "empty" || self.streamUrl == "empty" {
                self.service_id = self.defaults.string(forKey: "serviceId") ?? self.service_id
                self.phone = self.defaults.string(forKey: "authPhoneNum") ?? self.phone
                self.statusTextLabel.textColor = #colorLiteral(red: 0.9254902005, green: 0.2352941185, blue: 0.1019607857, alpha: 1)
                self.statusTextLabel.text = "Phone number unauthorized or invalid service ID. Phone No: \(self.phone), Service Id: \(self.service_id)."
                self.editPhoneBtn.isHidden = false
                self.editServiceIdBtn.isHidden = false
                self.continueBtn.isHidden = true
            }else{
                self.statusTextLabel.textColor = #colorLiteral(red: 0.3068847047, green: 0.7658930421, blue: 0.2954145416, alpha: 1)
                self.statusTextLabel.text = "You are ready for streaming, Click continue."
                self.editPhoneBtn.isHidden = true
                self.editServiceIdBtn.isHidden = true
                self.continueBtn.isHidden = false
            }
        }
        viewHiderOrEnabler.buttonCornerRadius(views: [editPhoneBtn, editServiceIdBtn, continueBtn])
        internetIdentifier()
    }
    

    //suporting only portrait orientation
    override var supportedInterfaceOrientations: UIInterfaceOrientationMask {
        return .portrait
    }
    
    //auto rotation false
    override var shouldAutorotate: Bool {
        return false
    }
    
    @IBAction func editServiceIdBtnAction(_ sender: UIButton) {
        if isInternetConnected {
            self.showSpinner(onView: self.view, msg: "Loading...")
            print("backServiceIdView")
            self.removeSpinner()
            self.grayColor(onView: self.statusView)
            self.performSegue(withIdentifier: "backServiceIdView", sender: Any?.self)
        }
    }
    
    @IBAction func editPhoneBtnAction(_ sender: UIButton) {
        if isInternetConnected {
            self.showSpinner(onView: self.view, msg: "Loading...")
            print("backPhoneView")
            self.removeSpinner()
            self.grayColor(onView: self.statusView)
            self.performSegue(withIdentifier: "backPhoneView", sender: Any?.self)
        }
    }
    
    
    @IBAction func continueBtnAction(_ sender: Any) {
         self.showSpinner(onView: self.view, msg: "Ready...")
         //print("continueing")
        self.removeSpinner()
        self.performSegue(withIdentifier: "cameraActivity", sender: Any?.self)
        self.grayColor(onView: self.statusView)
    }
    
    
    //used for identifying internet connection state
    open func internetIdentifier(){
        reachability.whenReachable = { _ in
            DispatchQueue.main.async {
                //self.connectionStatus.text = "connected"
            }
        }
        
        reachability.whenUnreachable = { _ in
            DispatchQueue.main.async {
                //self.connectionStatus.text = "not connected"
            }
        }
        
        NotificationCenter.default.addObserver(self, selector: #selector(internetChanged), name: Notification.Name.reachabilityChanged, object: reachability)
        do{
            try reachability.startNotifier()
        }catch{
            print("could not start notifier")
        }
    }
    
    //used for when internet connection state changed
    @objc func internetChanged(note: Notification) {
        let reachability = note.object as! Reachability
        if reachability.connection != .none {
            print("internetChanged: connected")
            whenDeviceOnline()
            isInternetConnected = true
        }else{
            whenDeviceOffline()
            print("internetChanged: not connected")
            isInternetConnected = false
        }
    }
    
    //used for when device is online
    func whenDeviceOnline(){
        snackbar.backgroundColor = #colorLiteral(red: 0.3411764801, green: 0.6235294342, blue: 0.1686274558, alpha: 1)
        snackbar.messageTextAlign = .center
        snackbar.message = "Internet Connected"
        DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + Double(Int64(3 * Double(NSEC_PER_SEC))) / Double(NSEC_PER_SEC)) {
            self.snackbar.dismiss()
        }
    }
    
    //used for when device is offline
    func whenDeviceOffline(){
        snackbar.backgroundColor = #colorLiteral(red: 0.2549019754, green: 0.2745098174, blue: 0.3019607961, alpha: 1)
        snackbar.messageTextAlign = .center
        snackbar.message = "No Internet Connection"
        snackbar.show()
    }
}
