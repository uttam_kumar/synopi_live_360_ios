//
//  CellTableViewCell.swift
//  SynopiLive360
//
//  Created by Syncrhonous on 5/5/19.
//  Copyright © 2019 Syncrhonous. All rights reserved.
//

//view of each cell of table view
import Foundation
class CellTableViewCell: UITableViewCell {
    
    @IBOutlet weak var flafImg: UIImageView!
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var codeLabel: UILabel!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
}
