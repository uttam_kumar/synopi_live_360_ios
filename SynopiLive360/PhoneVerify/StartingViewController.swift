//
//  StartingViewController.swift
//  SynopiLive360
//
//  Created by Syncrhonous on 4/7/19.
//  Copyright © 2019 Syncrhonous. All rights reserved.
//

import UIKit
import TTGSnackbar
import Foundation

class StartingViewController: UIViewController, UITextFieldDelegate {
    
    @IBOutlet weak var nameTextField: UITextField!
    @IBOutlet weak var nameOkBtn: UIButton!
    @IBOutlet var startingView: UIView!
    
    @IBOutlet weak var connectionStatus: UILabel!
    
    let defaults = UserDefaults.standard
    let reachability = Reachability()!
    let snackbar = TTGSnackbar(message: "TTGSnackbar !", duration: .forever)
    var isInternetConnected = true
    
    
    
    //orientation portrait
    override var supportedInterfaceOrientations: UIInterfaceOrientationMask {
        return .portrait
    }
    
    //auto rotation false
    override var shouldAutorotate: Bool {
        return false
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        internetIdentifier()
        self.defaults.set("StartingViewController", forKey: "currentViewController")
        //portrait orientation
        let value = UIInterfaceOrientation.portrait.rawValue
        UIDevice.current.setValue(value, forKey: "orientation")
        viewSetUp()
    }
    

    @IBAction func nameOkBtnAction(_ sender: UIButton) {
        if isInternetConnected{
            nameTextField.resignFirstResponder()
            self.defaults.set(nameTextField.text, forKey: "name")
            self.performSegue(withIdentifier: "phonePage", sender: Any?.self)
            self.grayColor(onView: self.startingView)
        }else{
            connectionStatus.shakeLabel()
        }
    }
    
    
    override func viewWillAppear(_ animated: Bool) {
        //left padding
        nameTextField.leftView = UIView(frame: CGRect(x: 0, y: 0, width: 12, height: nameTextField.frame.height))
        nameTextField.leftViewMode = .always
        //done btn of keyboard working from here through textFieldShouldReturn function
        nameTextField.delegate = self
        self.whenTextFieldInEmpty()
        //when changing text of phone number field
        nameTextField.addTarget(self, action: #selector(textFieldDidChange(sender:)), for: .editingChanged)
    }
    
    //starting view
    func viewSetUp(){
        nameOkBtn.layer.cornerRadius = 8
        self.nameTextField.layer.borderWidth = 1
        self.nameTextField.layer.borderColor = #colorLiteral(red: 0.501960814, green: 0.501960814, blue: 0.501960814, alpha: 1)
        self.nameTextField.layer.cornerRadius = 8
    }
    
    //when phone number field is in empty
    func whenTextFieldInEmpty(){
        self.nameTextField.text = ""
        self.nameOkBtn.setTitleColor(#colorLiteral(red: 0.6799238037, green: 0.3529885309, blue: 0.7658930421, alpha: 1), for: .normal)
        self.nameOkBtn.isEnabled = false
        self.nameOkBtn.layer.backgroundColor = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)
        self.nameOkBtn.layer.borderColor = #colorLiteral(red: 0.6799238037, green: 0.3529885309, blue: 0.7658930421, alpha: 1)
        self.nameOkBtn.layer.borderWidth = 1
    }
    
    //when phone number field is not empty
    func whenTextFieldNotEmpty(){
        self.nameOkBtn.isEnabled = true
        self.nameOkBtn.setTitleColor(#colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0), for: .normal)
        self.nameOkBtn.layer.borderWidth = 0
        self.nameOkBtn.layer.backgroundColor = #colorLiteral(red: 0.6799238037, green: 0.3529885309, blue: 0.7658930421, alpha: 1)
    }
    
    //textfield value changed
    @objc func textFieldDidChange(sender: UITextField) {
        let s:Int = nameTextField.text!.count
        print("counter: \(s)")
        DispatchQueue.main.async {
            //when phone number field is not empty
            if s > 0 {
                self.whenTextFieldNotEmpty()
            }
                //when phone number field is empty
            else{
                self.whenTextFieldInEmpty()
            }
        }
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool
    {
        print("done btnclicked")
        textField.resignFirstResponder()
        return true
    }
    
    
    //used for identifying internet connection state
    open func internetIdentifier(){
        reachability.whenReachable = { _ in
            DispatchQueue.main.async {
                //self.connectionStatus.text = "connected"
            }
        }
        
        reachability.whenUnreachable = { _ in
            DispatchQueue.main.async {
                //self.connectionStatus.text = "not connected"
            }
        }
        
        NotificationCenter.default.addObserver(self, selector: #selector(internetChanged), name: Notification.Name.reachabilityChanged, object: reachability)
        
        do{
            try reachability.startNotifier()
        }catch{
            print("could not start notifier")
        }
    }
    
    
    
    //used for when internet connection state changed
    @objc func internetChanged(note: Notification) {
        let reachability = note.object as! Reachability
        if reachability.connection != .none {
            print("internetChanged: connected")
            whenDeviceOnline()
            isInternetConnected = true
        }else{
            whenDeviceOffline()
            print("internetChanged: not connected")
            isInternetConnected = false
        }
    }
    
    //used for when device is online
    func whenDeviceOnline(){
        snackbar.backgroundColor = #colorLiteral(red: 0.3411764801, green: 0.6235294342, blue: 0.1686274558, alpha: 1)
        connectionStatus.textColor = #colorLiteral(red: 0.3411764801, green: 0.6235294342, blue: 0.1686274558, alpha: 1)
        snackbar.messageTextAlign = .center
        snackbar.message = "Internet Connected"
        connectionStatus.text = "Enter your name"
        DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + Double(Int64(3 * Double(NSEC_PER_SEC))) / Double(NSEC_PER_SEC)) {
            self.snackbar.dismiss()
        }
    }
    
    
    //used for when device is offline
    func whenDeviceOffline(){
        connectionStatus.text = "No internet connection"
        connectionStatus.textColor = #colorLiteral(red: 0.9254902005, green: 0.2352941185, blue: 0.1019607857, alpha: 1)
        snackbar.backgroundColor = #colorLiteral(red: 0.2549019754, green: 0.2745098174, blue: 0.3019607961, alpha: 1)
        snackbar.messageTextAlign = .center
        snackbar.message = "No Internet Connection"
        snackbar.show()
    }
    
}
