//
//  MainViewController.swift
//  SynopiLive360
//
//  Created by Syncrhonous on 5/5/19.
//  Copyright © 2019 Syncrhonous. All rights reserved.
//


//phone number verification part (select country code and enter phone number to send code)
import UIKit
import FirebaseAuth
import TTGSnackbar
import Photos

class MainViewController: UIViewController {
    
    let defaults = UserDefaults.standard
    let reachability = Reachability()!
    let snackbar = TTGSnackbar(message: "TTGSnackbar !", duration: .forever)
    var isInternetConnected = true
    
    
    @IBOutlet var mainV: UIView!
    @IBOutlet weak var countryPickerView: UIView!
    @IBOutlet weak var phoneNumberField: UITextField!
    @IBOutlet weak var statusTextLabel: UILabel!
    @IBOutlet weak var countryNameLabel: UILabel!
    @IBOutlet weak var nextBtn: UIButton!
    @IBOutlet weak var countryFlagImage: UIImageView!
    
    var countryPicker: String = "Bangladesh"
    var countryPickerCode: String = "+880"
    var countryPickerFlag: String = "bangladesh"
    
    //when country selected then refresh label of country pickerview
    @objc func refreshLbl(notification: NSNotification) {
        countryPicker = defaults.string(forKey: "CountryName") ?? "Bangladesh"
        countryPickerCode = defaults.string(forKey: "CountryCode") ?? "+880"
        countryNameLabel.text = "\(countryPicker) (\(countryPickerCode))"
        countryPickerFlag = defaults.string(forKey: "CountryFlag") ?? "bangladesh"
        countryFlagImage.image = UIImage(named: countryPickerFlag)
        print(countryPickerFlag)
    }
    
    //orientation portrait
    override var supportedInterfaceOrientations: UIInterfaceOrientationMask {
        return .portrait
    }
    
    //auto rotation false
    override var shouldAutorotate: Bool {
        return false
    }
    

    
    override func viewDidLoad() {
        super.viewDidLoad()
        nextBtn.layer.cornerRadius = 8
        internetIdentifier()

        self.defaults.set("MainViewController", forKey: "currentViewController")
       //portrait orientation
        let value = UIInterfaceOrientation.portrait.rawValue
        UIDevice.current.setValue(value, forKey: "orientation")
        //setting up layout view
        viewSetUp()
        //clicking country picker view
        let countryPicker = UITapGestureRecognizer(target: self, action: #selector(countryPickerViewAction(_:)))
        self.countryPickerView.addGestureRecognizer(countryPicker)
        //when country selected then refresh label of country pickerview
        NotificationCenter.default.addObserver(self,selector: #selector(refreshLbl), name: NSNotification.Name(rawValue:  "refresh"),object: nil)
    }
    
    //country picker view clicked
    @objc func countryPickerViewAction(_ sender:UITapGestureRecognizer){
        let customAlert = self.storyboard?.instantiateViewController(withIdentifier: "countryCodePicker") as! CodePickerViewController
        customAlert.providesPresentationContextTransitionStyle = true
        customAlert.definesPresentationContext = true
        customAlert.modalPresentationStyle = UIModalPresentationStyle.overCurrentContext
        customAlert.modalTransitionStyle = UIModalTransitionStyle.crossDissolve
        //customAlert.delegate = self
        self.present(customAlert, animated: true, completion: nil)
    }
    
    
    //when next button clicked to send code
    @IBAction func codeSendNextBthAction(_ sender: UIButton) {
        print("code send btn clicked")
        if isInternetConnected{
            //when phone number field is not empty
            if(!phoneNumberField.text!.isEmpty){
                
                let pNum = self.phoneNumberField.text!
                var phoneEx = countryPickerCode
                phoneNumberField.resignFirstResponder()
                if (countryPickerCode == "+880"){
                    if (pNum.prefix(1) == "0"){
                        phoneEx = "+88"
                    }
                }
                
                
                let phoneNum: String = phoneEx + self.phoneNumberField.text!
                print("pp: \(phoneNum)")
                
                
                let alert = UIAlertController(title: "", message: "", preferredStyle: .alert)
                
                //to change font of title and message.
                let titleFont = [NSAttributedString.Key.font: UIFont(name: "Raleway-Bold", size: 19.0)!]
                let messageFont = [NSAttributedString.Key.font: UIFont(name: "Raleway-Regular", size: 16.0)!]
                let titleAttrString = NSMutableAttributedString(string: "Attention", attributes: titleFont)
                let messageAttrString = NSMutableAttributedString(string: "We will be verify the phone number \(phoneNum). Is this a valid phone number?", attributes: messageFont)
                alert.setValue(titleAttrString, forKey: "attributedTitle")
                alert.setValue(messageAttrString, forKey: "attributedMessage")
                
            
                let action = UIAlertAction(title: "Yes", style: .default){ (UIAlertAction) in
                    if self.isInternetConnected {
                        self.showSpinner(onView: self.view, msg: "Sending Code...")
                        
                        if phoneNum == "+880777888999"//"+8801779722399"//
                        {
                            //when code sending successful
                            self.phoneAuthSuccess()
                            self.defaults.set(phoneNum, forKey: "authPhoneNum")
                            self.defaults.set("123456", forKey: "authVID")
                            self.performSegue(withIdentifier: "code", sender: Any?.self)
                            self.grayColor(onView: self.mainV)
                        }else{
                            //when yes button clicked
                            PhoneAuthProvider.provider().verifyPhoneNumber(phoneNum, uiDelegate: nil) { (verificationID, error) in
                                if error != nil {
                                    //when can not send code
                                    self.phoneAuthError()
                                    print("error to send code")
                                    print("Auth Error: \(String(describing: error?.localizedDescription))")
                                }else{
                                    //when code sending successful
                                    self.phoneAuthSuccess()
                                    self.defaults.set(phoneNum, forKey: "authPhoneNum")
                                    self.defaults.set(verificationID, forKey: "authVID")
                                    self.performSegue(withIdentifier: "code", sender: Any?.self)
                                    self.grayColor(onView: self.mainV)
                                }
                            }
                        }
                    }else{
                        self.statusTextLabel.shakeLabel()
                    }
                }
                
                //when edit button clicked
                let cancel = UIAlertAction(title: "Edit", style: .cancel, handler: nil)
                
                action.setValue(UIColor.init(cgColor: #colorLiteral(red: 0.6799238037, green: 0.3529885309, blue: 0.7658930421, alpha: 1)), forKey: "titleTextColor")
                cancel.setValue(UIColor.init(cgColor: #colorLiteral(red: 1, green: 0.3706036828, blue: 0.2044453019, alpha: 1)), forKey: "titleTextColor")
                alert.addAction(action)
                alert.addAction(cancel)
                self.present(alert, animated: true, completion: nil)
            }
                //when phone number field is empty
            else{
                self.statusTextLabel.textColor = #colorLiteral(red: 0.9254902005, green: 0.2352941185, blue: 0.1019607857, alpha: 1)
                self.statusTextLabel.text = "Empty phone number field"
                print("empty phone number field")
                self.removeSpinner()
            }
        }else{
            statusTextLabel.shakeLabel()
        }
    }
    
    

    
    override func viewWillAppear(_ animated: Bool) {
        //left padding
        phoneNumberField.leftView = UIView(frame: CGRect(x: 0, y: 0, width: 12, height: phoneNumberField.frame.height))
        phoneNumberField.leftViewMode = .always
        
        self.whenTextFieldInEmpty()
        //when changing text of phone number field
        phoneNumberField.addTarget(self, action: #selector(textFieldDidChange(sender:)), for: .editingChanged)
    }
    

    //when can send code
    func phoneAuthSuccess(){
        self.statusTextLabel.textColor = #colorLiteral(red: 0.3068847047, green: 0.7658930421, blue: 0.2954145416, alpha: 1)
        self.statusTextLabel.text = "Code sent"
        print("code sent")
        self.removeSpinner()
    }
    
    
    //when error to send code
    func phoneAuthError(){
        self.statusTextLabel.textColor = #colorLiteral(red: 0.9254902005, green: 0.2352941185, blue: 0.1019607857, alpha: 1)
        //if internet connected
        if self.isInternetConnected {
            self.statusTextLabel.text = "Invalid phone number"
            self.removeSpinner()
        }
        //if internet not connected
        else{
            self.statusTextLabel.text = "No internet connection"
            self.removeSpinner()
        }
        
    }
    
    //starting view
    func viewSetUp(){
        //country picker view
        self.countryPickerView.layer.borderWidth = 1
        self.countryPickerView.layer.borderColor = #colorLiteral(red: 0.501960814, green: 0.501960814, blue: 0.501960814, alpha: 1)
        self.countryPickerView.layer.cornerRadius = 8
        //phone number field view
        self.phoneNumberField.layer.borderWidth = 1
        self.phoneNumberField.layer.borderColor = #colorLiteral(red: 0.501960814, green: 0.501960814, blue: 0.501960814, alpha: 1)
        self.phoneNumberField.layer.cornerRadius = 8
    }
    
    //when phone number field is in empty
    func whenTextFieldInEmpty(){
        self.phoneNumberField.text = ""
        self.nextBtn.setTitleColor(#colorLiteral(red: 0.6799238037, green: 0.3529885309, blue: 0.7658930421, alpha: 1), for: .normal)
        self.nextBtn.isEnabled = false
        self.nextBtn.layer.backgroundColor = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)
        self.nextBtn.layer.borderColor = #colorLiteral(red: 0.6799238037, green: 0.3529885309, blue: 0.7658930421, alpha: 1)
        self.nextBtn.layer.borderWidth = 1
    }
    
    //when phone number field is not empty
    func whenTextFieldNotEmpty(){
        self.nextBtn.isEnabled = true
        self.nextBtn.setTitleColor(#colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0), for: .normal)
        if isInternetConnected{
            self.statusTextLabel.text = "Verify phone"
            self.statusTextLabel.textColor = #colorLiteral(red: 0.3068847047, green: 0.7658930421, blue: 0.2954145416, alpha: 1)
        }else{
            self.statusTextLabel.textColor = #colorLiteral(red: 0.9254902005, green: 0.2352941185, blue: 0.1019607857, alpha: 1)
            self.statusTextLabel.text = "No internet connection"
        }
        
        self.nextBtn.layer.borderWidth = 0
        self.nextBtn.layer.backgroundColor = #colorLiteral(red: 0.6799238037, green: 0.3529885309, blue: 0.7658930421, alpha: 1)
    }
    
    //textfield value changed
    @objc func textFieldDidChange(sender: UITextField) {
        let s:Int = phoneNumberField.text!.count
        print("counter: \(s)")
        DispatchQueue.main.async {
            //when phone number field is not empty
            if s > 0 {
                self.whenTextFieldNotEmpty()
            }
            //when phone number field is empty
            else{
                self.whenTextFieldInEmpty()
            }
        }
    }
    
    
    //used for identifying internet connection state
    open func internetIdentifier(){
        reachability.whenReachable = { _ in
            DispatchQueue.main.async {
                //self.connectionStatus.text = "connected"
            }
        }
        
        reachability.whenUnreachable = { _ in
            DispatchQueue.main.async {
                //self.connectionStatus.text = "not connected"
            }
        }
        
        NotificationCenter.default.addObserver(self, selector: #selector(internetChanged), name: Notification.Name.reachabilityChanged, object: reachability)
        
        do{
            try reachability.startNotifier()
        }catch{
            print("could not start notifier")
        }
    }
    
  
    
    //used for when internet connection state changed
    @objc func internetChanged(note: Notification) {
        let reachability = note.object as! Reachability
        if reachability.connection != .none {
            print("internetChanged: connected")
            whenDeviceOnline()
            isInternetConnected = true
        }else{
            whenDeviceOffline()
            print("internetChanged: not connected")
            isInternetConnected = false
        }
    }
    
    //used for when device is online
    func whenDeviceOnline(){
        snackbar.backgroundColor = #colorLiteral(red: 0.3411764801, green: 0.6235294342, blue: 0.1686274558, alpha: 1)
        snackbar.messageTextAlign = .center
        snackbar.message = "Internet Connected"
        statusTextLabel.text = "Verify phone"
        statusTextLabel.textColor = #colorLiteral(red: 0.3068847047, green: 0.7658930421, blue: 0.2954145416, alpha: 1)
        DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + Double(Int64(3 * Double(NSEC_PER_SEC))) / Double(NSEC_PER_SEC)) {
            self.snackbar.dismiss()
        }
    }
    
    
    //used for when device is offline
    func whenDeviceOffline(){
        snackbar.backgroundColor = #colorLiteral(red: 0.2549019754, green: 0.2745098174, blue: 0.3019607961, alpha: 1)
        statusTextLabel.textColor = #colorLiteral(red: 0.9254902005, green: 0.2352941185, blue: 0.1019607857, alpha: 1)
        snackbar.messageTextAlign = .center
        statusTextLabel.text = "No internet connection"
        snackbar.message = "No Internet Connection"
        snackbar.show()
    }
    
}
