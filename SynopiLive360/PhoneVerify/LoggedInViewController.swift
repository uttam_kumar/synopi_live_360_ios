//
//  LoggedInViewController.swift
//  SynopiLive360
//
//  Created by Syncrhonous on 8/5/19.
//  Copyright © 2019 Syncrhonous. All rights reserved.
//


//after varified phone number it needs to enter a service id 
import UIKit
import TTGSnackbar
import CommonCrypto
import FirebaseAuth
import Foundation

class LoggedInViewController: UIViewController {
    let reachability = Reachability()!
    let snackbar = TTGSnackbar(message: "TTGSnackbar !", duration: .forever)
    var isInternetConnected = true
    
    @IBOutlet var serviceMainV: UIView!
    @IBOutlet weak var successfulTextLabel: UILabel!
    @IBOutlet weak var serviceIdTextField: UITextField!
    @IBOutlet weak var serviceOkBtn: UIButton!
    
    let defaults = UserDefaults.standard
    var token: String = "asdfghjklpoiuytrewqxcvbnmasdfghjklpoiuytrewqxcvbnmasdfghjklpoiuytrewqxcvbnmasdfghjklpoiuytrewqxcvbnmasdfghjklpoiuytrewqxcvbnmasdfghjklpoiuytrewqxcvbnmasdfghjklpoiuytrewqxcvbnmasdfghjklpoiuytrewqxcvbnm"
    var phoneNum: String = ""
    var s_id: String = ""
    var streamUrl: String = ""
    var streamKey: String = ""
    
    var crypto: Crypto? = nil
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.removeSpinner()
        //portrait orientation
        let value = UIInterfaceOrientation.portrait.rawValue
        UIDevice.current.setValue(value, forKey: "orientation")
        internetIdentifier()
        self.defaults.set("LoggedInViewController", forKey: "currentViewController")
        //setting up views
        viewSettingUp()
        //left padding
        serviceIdTextField.leftView = UIView(frame: CGRect(x: 0, y: 0, width: 12, height: serviceIdTextField.frame.height))
        serviceIdTextField.leftViewMode = .always
        //when editing service id text field
        serviceIdTextField.addTarget(self, action: #selector(textFieldDidChange(sender:)), for: .editingChanged)
    }
    
    //suporting only portrait orientation
    override var supportedInterfaceOrientations: UIInterfaceOrientationMask {
        return .portrait
    }
    
    //auto rotation false
    override var shouldAutorotate: Bool {
        return false
    }
    
    
    //settings up views when opens controller
    func viewSettingUp(){
        phoneNum = defaults.string(forKey: "authPhoneNum")!
        print("phoneNum: \(phoneNum)")
        token = self.defaults.string(forKey: "token") ?? token
        print("token111: \(token)")
        self.serviceOkBtn.layer.cornerRadius = 8
        self.serviceIdTextField.layer.borderWidth = 1
        self.serviceIdTextField.layer.borderColor = #colorLiteral(red: 0.501960814, green: 0.501960814, blue: 0.501960814, alpha: 1)
        self.serviceIdTextField.layer.cornerRadius = 8
        
        self.serviceOkBtn.setTitleColor(#colorLiteral(red: 0.6799238037, green: 0.3529885309, blue: 0.7658930421, alpha: 1), for: .normal)
        self.serviceOkBtn.isEnabled = false
        self.serviceOkBtn.layer.backgroundColor = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)
        self.serviceOkBtn.layer.borderColor = #colorLiteral(red: 0.6799238037, green: 0.3529885309, blue: 0.7658930421, alpha: 1)
        self.serviceOkBtn.layer.borderWidth = 1
    }

    //service id ok button clicked
    @IBAction func serviceIdOkBtnAction(_ sender: UIButton) {
        if(isInternetConnected){
            self.showSpinner(onView: self.view, msg: "Preparing...")
            serviceIdTextField.resignFirstResponder()
            s_id = serviceIdTextField.text!
            defaults.setValue(s_id, forKey: "serviceId")
            print("s_id: \(s_id)")
            DispatchQueue.main.async {
                self.startHttpRquest(token: self.token)
            }
        }else{
            successfulTextLabel.shakeLabel()
            //successfulTextLabel.text = "No internet connection"
        }
    }
    
    //textfield value changed
    @objc func textFieldDidChange(sender: UITextField) {
        let s:Int = serviceIdTextField.text!.count
        print("counter: \(s)")
        DispatchQueue.main.async {
            //if service id text field not empty
            if s > 0  {
                self.whenTextFieldNotEmpty()
            }
            //if service id text field is empty
            else{
                self.whenTextFieldInEmpty()
            }
        }
    }
    
    
    //when service id text field is in empty
    func whenTextFieldInEmpty(){
        self.serviceOkBtn.setTitleColor(#colorLiteral(red: 0.6799238037, green: 0.3529885309, blue: 0.7658930421, alpha: 1), for: .normal)
        self.serviceOkBtn.isEnabled = false
        self.serviceOkBtn.layer.backgroundColor = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)
        self.serviceOkBtn.layer.borderColor = #colorLiteral(red: 0.6799238037, green: 0.3529885309, blue: 0.7658930421, alpha: 1)
        self.serviceOkBtn.layer.borderWidth = 1
    }
    
    //when service id text field is not empty
    func whenTextFieldNotEmpty(){
        self.serviceOkBtn.isEnabled = true
        self.serviceOkBtn.setTitleColor(#colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0), for: .normal)
        //if internet conected
        if self.isInternetConnected {
            self.successfulTextLabel.textColor = #colorLiteral(red: 0.3068847047, green: 0.7658930421, blue: 0.2954145416, alpha: 1)
            self.successfulTextLabel.text = "Verification successful"
        }
        //if internet not connected
        else{
            self.successfulTextLabel.textColor = #colorLiteral(red: 0.9254902005, green: 0.2352941185, blue: 0.1019607857, alpha: 1)
            self.successfulTextLabel.text = "No internet connection"
        }
        
        self.serviceOkBtn.layer.borderWidth = 0
        self.serviceOkBtn.layer.backgroundColor = #colorLiteral(red: 0.6799238037, green: 0.3529885309, blue: 0.7658930421, alpha: 1)
    }
    
    
    
    //used for identifying internet connection state
    open func internetIdentifier(){
        reachability.whenReachable = { _ in
            DispatchQueue.main.async {
                //self.connectionStatus.text = "connected"
            }
        }
        
        reachability.whenUnreachable = { _ in
            DispatchQueue.main.async {
                //self.connectionStatus.text = "not connected"
            }
        }
        
        NotificationCenter.default.addObserver(self, selector: #selector(internetChanged), name: Notification.Name.reachabilityChanged, object: reachability)
        
        do{
            try reachability.startNotifier()
        }catch{
            print("could not start notifier")
        }
    }
    
    //used for when internet connection state changed
    @objc func internetChanged(note: Notification) {
        let reachability = note.object as! Reachability
        if reachability.connection != .none {
            print("internetChanged: connected")
            whenDeviceOnline()
            isInternetConnected = true
        }else{
            whenDeviceOffline()
            print("internetChanged: not connected")
            isInternetConnected = false
        }
    }
    
    //used for when device is online
    func whenDeviceOnline(){
        snackbar.backgroundColor = #colorLiteral(red: 0.3411764801, green: 0.6235294342, blue: 0.1686274558, alpha: 1)
        snackbar.messageTextAlign = .center
        snackbar.message = "Internet Connected"
        successfulTextLabel.text = "Verification successful"
        successfulTextLabel.textColor = #colorLiteral(red: 0.3068847047, green: 0.7658930421, blue: 0.2954145416, alpha: 1)
        DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + Double(Int64(3 * Double(NSEC_PER_SEC))) / Double(NSEC_PER_SEC)) {
            self.snackbar.dismiss()
        }
    }
    
    //used for when device is offline
    func whenDeviceOffline(){
        print("nnnnn: nointernet")
        snackbar.backgroundColor = #colorLiteral(red: 0.2549019754, green: 0.2745098174, blue: 0.3019607961, alpha: 1)
        successfulTextLabel.textColor = #colorLiteral(red: 0.9254902005, green: 0.2352941185, blue: 0.1019607857, alpha: 1)
        snackbar.messageTextAlign = .center
        successfulTextLabel.text = "No internet connection"
        snackbar.message = "No Internet Connection"
        snackbar.show()
    }
    
    //MD5 message digestion
    func MD5(_ data: Data) -> Data {
        let length = Int(CC_MD5_DIGEST_LENGTH)
        var digest = Data(count: length)
        
        data.withUnsafeBytes {bytes in
            _ = digest.withUnsafeMutableBytes {mutableBytes in
                CC_MD5(bytes.baseAddress, CC_LONG(bytes.count), mutableBytes.bindMemory(to: UInt8.self).baseAddress)
            }
        }
        return digest
    }
    
    //http request here
    func startHttpRquest(token: String){
        let strongToken = "YourStrongKey" + token
        print("strongToken: \(strongToken)")
        print("token length: \(token.count)")
        let tokenRes = self.MD5(strongToken.data(using: .utf8)!)
        self.crypto = Crypto(tokenRes)
        let msg = [
            "phone": phoneNum,
            "shortcode": s_id,
        ]
        let msgData = try! JSONSerialization.data(withJSONObject: msg)
        
        let ss = String(data: msgData, encoding: .utf8)!
        print(ss)
        let eneMsg = self.crypto!.encrypt(ss)!
        print(eneMsg)
        
        let reque = "http://seeds.synopi.com:8002/resolve/\(eneMsg.hexEncodedString())/\(token)"
        print("\n\nurlFirst: \(reque)")
  
        httpRequester(url: reque)
    }
    
    
    
    func httpRequester(url: String){
        let myUrl = NSURL(string: url)
        let request = NSMutableURLRequest(url: myUrl! as URL)
        request.httpMethod = "GET"
        var responseString = "Not Found"
        let task = URLSession.shared.dataTask(with: request as URLRequest) {
            data, response, error in
            
            if error != nil {
                print("response string = error")
                self.notFoundUrl()
            }else {
                responseString = String(data: data!, encoding: String.Encoding(rawValue: String.Encoding.utf8.rawValue))!
                //print("responseString1 = \(responseString)")
                if responseString == "Not Found" {
                    print("response string = not found")
                    self.notFoundUrl()
                }else{
                    print("response string = :\(responseString)")
                    let dat: Data = self.hexStringToData(responseString)
                    let dec = (self.crypto?.decrypt(dat, 0, dat.count)  ?? nil)
                    DispatchQueue.main.async {
                        if dec != nil {
                            do{
                                let jsonOb = try JSONSerialization.jsonObject(with: dec!, options: [])
                                let api_url = (jsonOb as AnyObject).value(forKey: "api_url") as? String
                                if api_url != nil {
                                    self.onAppApiUrl(api_url: api_url!)
                                }
                                let rtmp_url = (jsonOb as AnyObject).value(forKey: "rtmp_url") as? String
                                if rtmp_url != nil {
                                    print(rtmp_url!)
                                    let splashIndex = rtmp_url!.lastIndex(of: "/")
                                    let indexValue = rtmp_url!.distance(from: rtmp_url!.startIndex, to: splashIndex!) + 1
                                    let s_url = rtmp_url!.prefix(indexValue)
                                    let s_key = rtmp_url!.dropFirst(indexValue)
                                    self.updateStatusActivity(url: String(s_url), key: String(s_key))
                                }
                            }catch{
                                print(error.localizedDescription)
                            }
                        }
                    }
                }
            }
        }
        task.resume()
    }
    
    func onAppApiUrl(api_url: String){
        print("method: onApiUrl")
        let msg = [
            "phone": phoneNum,
        ]
        let msgData = try! JSONSerialization.data(withJSONObject: msg)
        
        let ss = String(data: msgData, encoding: .utf8)!
        print(ss)
        let eneMsg = self.crypto!.encrypt(ss)!
        print(eneMsg)
        
        let reques = "\(api_url)/\(eneMsg.hexEncodedString())/\(token)"
        print("\nurlFirst 2: \(reques)")
        httpRequester(url: reques)
    }
    
    
    func updateStatusActivity(url: String, key: String){
        let streamUrl = url
        let streamKey = key
        defaults.setValue(streamUrl, forKey: "publishUrl")
        defaults.setValue(streamKey, forKey: "publishKey")
        DispatchQueue.main.async {
            self.performSegue(withIdentifier: "statusActivity", sender: Any?.self)
        }
        self.removeSpinner()
        self.grayColor(onView: self.serviceMainV)
    }
    
    func notFoundUrl(){
        defaults.setValue("empty", forKey: "publishUrl")
        defaults.setValue("empty", forKey: "publishKey")
        DispatchQueue.main.async {
            self.performSegue(withIdentifier: "statusActivity", sender: Any?.self)
        }
        self.removeSpinner()
        self.grayColor(onView: self.serviceMainV)
    }


    //converting hex string to data
    func hexStringToData(_ s: String) -> Data {
        let len = s.count
        var data = Data(count: len/2)
        var start = s.startIndex
        for i in 0..<len/2 {
            let end = s.index(start, offsetBy: 2)
            data[i] = UInt8(s[start..<end], radix: 16)!
            start = end
        }
        return data
    }
    
}

//converting data to hex string
extension Data {
    func hexEncodedString() -> String {
        return map { String(format: "%02hhx", $0) }.joined()
    }
}
