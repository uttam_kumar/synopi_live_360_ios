//
//  OtpCodeViewController.swift
//  SynopiLive360
//
//  Created by Syncrhonous on 8/5/19.
//  Copyright © 2019 Syncrhonous. All rights reserved.
//


//otp code verification view
import UIKit
import FirebaseAuth
import TTGSnackbar
import Firebase
import FirebaseCore
import UserNotifications

class OtpCodeViewController: UIViewController {
    let reachability = Reachability()!
    let snackbar = TTGSnackbar(message: "TTGSnackbar !", duration: .forever)
    var isInternetConnected = true

    
    @IBOutlet var codeMainV: UIView!
    @IBOutlet weak var codeTextField: UITextField!
    @IBOutlet weak var statusVerifyTextLabel: UILabel!
    @IBOutlet weak var resendCodeBtn: UIButton!
    @IBOutlet weak var verifyCodeBtn: UIButton!
    @IBOutlet weak var timeCounterLabel: UILabel!
    let defaults = UserDefaults.standard
    
    
    var vtoken: String = "asdfghjklpoiuytrewqxcvbnmasdfghjklpoiuytrewqxcvbnmasdfghjklpoiuytrewqxcvbnmasdfghjklpoiuytrewqxcvbnmasdfghjklpoiuytrewqxcvbnmasdfghjklpoiuytrewqxcvbnmasdfghjklpoiuytrewqxcvbnmasdfghjklpoiuytrewqxcvbnm"
    var phoneNum: String = ""
    var timer = Timer()
    var totalSecond = 90
    var readyToken: String = ""
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        //disable resend code button
        resendBtnStyleWhenDisable()
        self.defaults.set("OtpCodeViewController", forKey: "currentViewController")
        //portrait orientation
        let value = UIInterfaceOrientation.portrait.rawValue
        UIDevice.current.setValue(value, forKey: "orientation")
        //view set up
        viewSettingUp()
        internetIdentifier()
        print("verification code view controller")
        // Do any additional setup after loading the view.
    }
    
    //support portrait orientation
    override var supportedInterfaceOrientations: UIInterfaceOrientationMask {
        return .portrait
    }
    
    //auto rotation false
    override var shouldAutorotate: Bool {
        return false
    }
    
    //view set up
    func viewSettingUp(){
        resendCodeBtn.layer.cornerRadius = 8
        verifyCodeBtn.layer.cornerRadius = 8
        phoneNum = defaults.string(forKey: "authPhoneNum")!
        self.statusVerifyTextLabel.textColor = #colorLiteral(red: 0.3068847047, green: 0.7658930421, blue: 0.2954145416, alpha: 1)
        self.statusTextChanged(text: "Code sent to \(phoneNum)")
        
        self.codeTextField.layer.borderWidth = 1
        self.codeTextField.layer.borderColor = #colorLiteral(red: 0.501960814, green: 0.501960814, blue: 0.501960814, alpha: 1)
        self.codeTextField.layer.cornerRadius = 8
        
        
        verifyCodeBtn.isEnabled = false
        codeTextField.text = ""
        verifyCodeBtn.layer.backgroundColor = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)
        verifyCodeBtn.layer.borderColor = #colorLiteral(red: 0.6799238037, green: 0.3529885309, blue: 0.7658930421, alpha: 1)
        verifyCodeBtn.layer.borderWidth = 1
        self.verifyCodeBtn.setTitleColor(#colorLiteral(red: 0.6799238037, green: 0.3529885309, blue: 0.7658930421, alpha: 1), for: .normal)
    }
    
    //resend and verify button style when resend button disabled
    func resendBtnStyleWhenDisable(){
        self.resendCodeBtn.setTitleColor(#colorLiteral(red: 0.6799238037, green: 0.3529885309, blue: 0.7658930421, alpha: 1), for: .normal)
        self.resendCodeBtn.isEnabled = false
        self.resendCodeBtn.layer.backgroundColor = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)
        self.resendCodeBtn.layer.borderColor = #colorLiteral(red: 0.6799238037, green: 0.3529885309, blue: 0.7658930421, alpha: 1)
        self.resendCodeBtn.layer.borderWidth = 1
        self.verifyCodeBtn.isEnabled = true
        self.verifyCodeBtn.layer.backgroundColor = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)
        self.verifyCodeBtn.layer.borderColor = #colorLiteral(red: 0.6799238037, green: 0.3529885309, blue: 0.7658930421, alpha: 1)
        self.verifyCodeBtn.layer.borderWidth = 1
        self.verifyCodeBtn.setTitleColor(#colorLiteral(red: 0.6799238037, green: 0.3529885309, blue: 0.7658930421, alpha: 1), for: .normal)
    }
    
    //resend and verify button style when resend button enabled
    func resendBtnStyleWhenEnable(){
        self.resendCodeBtn.setTitleColor(#colorLiteral(red: 1, green: 1, blue: 1, alpha: 1), for: .normal)
        self.resendCodeBtn.isEnabled = true
        self.resendCodeBtn.layer.backgroundColor = #colorLiteral(red: 0.6799238037, green: 0.3529885309, blue: 0.7658930421, alpha: 1)
        self.resendCodeBtn.layer.borderWidth = 0
        self.verifyCodeBtn.isEnabled = false
    }
    
    override func viewWillAppear(_ animated: Bool) {
        //count down counter start counting for code valid time
        startTimer()
        //padding of xode text field
        codeTextField.leftView = UIView(frame: CGRect(x: 0, y: 0, width: 12, height: codeTextField.frame.height))
        codeTextField.leftViewMode = .always
  
        //when code text field strat editing
        codeTextField.addTarget(self, action: #selector(textFieldDidChange(sender:)), for: .editingChanged)
    }
    
    
    //codeTextField value changed
    @objc func textFieldDidChange(sender: UITextField) {
        let s:Int = codeTextField.text!.count
        print("counter: \(s)")
        DispatchQueue.main.async {
            //when code text field is not empty
            if s > 0 && self.totalSecond > 0 {
                self.whenTextFieldNotEmpty()
            }
            //when code text field is empty
            else{
                self.whenTextFieldInEmpty()
            }
        }
        
    }
    
    
    //when code text field is in empty
    func whenTextFieldInEmpty(){
        self.verifyCodeBtn.setTitleColor(#colorLiteral(red: 0.6799238037, green: 0.3529885309, blue: 0.7658930421, alpha: 1), for: .normal)
        self.verifyCodeBtn.isEnabled = false
        self.verifyCodeBtn.layer.backgroundColor = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)
        self.verifyCodeBtn.layer.borderColor = #colorLiteral(red: 0.6799238037, green: 0.3529885309, blue: 0.7658930421, alpha: 1)
        self.verifyCodeBtn.layer.borderWidth = 1
    }
    
    //when code text field is not empty
    func whenTextFieldNotEmpty(){
        self.verifyCodeBtn.isEnabled = true
        self.verifyCodeBtn.setTitleColor(#colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0), for: .normal)
        if self.isInternetConnected {
            self.statusVerifyTextLabel.textColor = #colorLiteral(red: 0.3068847047, green: 0.7658930421, blue: 0.2954145416, alpha: 1)
            self.statusVerifyTextLabel.text = "Code sent to \(self.phoneNum)"
        }else{
            self.statusVerifyTextLabel.textColor = #colorLiteral(red: 0.9254902005, green: 0.2352941185, blue: 0.1019607857, alpha: 1)
            self.statusVerifyTextLabel.text = "No Internet Connection"
        }
        
        self.verifyCodeBtn.layer.borderWidth = 0
        self.verifyCodeBtn.layer.backgroundColor = #colorLiteral(red: 0.6799238037, green: 0.3529885309, blue: 0.7658930421, alpha: 1)
    }
    
    //resend button click
    @IBAction func resendCodeBtnAction(_ sender: UIButton) {
        if isInternetConnected {
            self.showSpinner(onView: self.view, msg: "Resending Code...")
            codeTextField.text = ""
            self.statusVerifyTextLabel.textColor = #colorLiteral(red: 0.3068847047, green: 0.7658930421, blue: 0.2954145416, alpha: 1)
            
            if phoneNum == "+880777888999" //"+8801779722399" //
            {
                self.defaults.set(self.phoneNum, forKey: "authPhoneNum")
                self.defaults.set("123456", forKey: "authVID")
                self.resendingCodeSuccessfully()
            }else{
                PhoneAuthProvider.provider().verifyPhoneNumber(phoneNum, uiDelegate: nil) { (verificationID, error) in
                    //when error to send code
                    if error != nil {
                        self.statusVerifyTextLabel.textColor = #colorLiteral(red: 0.9254902005, green: 0.2352941185, blue: 0.1019607857, alpha: 1)
                        self.statusTextChanged(text: "Error to send code")
                        self.removeSpinner()
                        print("Auth Error: \(String(describing: error?.localizedDescription))")
                    }
                        //when successfullly resend code
                    else{
                        self.defaults.set(self.phoneNum, forKey: "authPhoneNum")
                        self.defaults.set(verificationID, forKey: "authVID")
                        self.resendingCodeSuccessfully()
                    }
                }
            }
        }else{
            self.statusVerifyTextLabel.shakeLabel()
        }
    }
    
    //update view when code successfully resend
    func resendingCodeSuccessfully(){
        self.statusVerifyTextLabel.textColor = #colorLiteral(red: 0.3068847047, green: 0.7658930421, blue: 0.2954145416, alpha: 1)
        self.statusVerifyTextLabel.text = "Code resent to \(self.phoneNum)"
        self.totalSecond = 90
        self.startTimer()
        self.resendBtnStyleWhenDisable()
        self.removeSpinner()
    }
    
    //verify button clicked
    @IBAction func verifyCodeBtnAction(_ sender: UIButton) {
        if isInternetConnected{
            codeTextField.resignFirstResponder()
            self.showSpinner(onView: self.view, msg: "Verifying...")
            self.statusVerifyTextLabel.textColor = #colorLiteral(red: 0.3068847047, green: 0.7658930421, blue: 0.2954145416, alpha: 1)
            self.statusVerifyTextLabel.text = "Verifying OTP code"
            let otpCode = codeTextField.text!
            print("verify btn clicked: \(otpCode)")
            //when code text field is empty
            if (otpCode == ""){
                print("empty otp code")
                self.statusVerifyTextLabel.textColor = #colorLiteral(red: 0.9254902005, green: 0.2352941185, blue: 0.1019607857, alpha: 1)
                self.statusTextChanged(text: "Empty OTP code")
                self.removeSpinner()
            }
                //when code text field not empty
            else{
                if phoneNum == "+880777888999" //"+8801779722399"
                {
                    if(codeTextField.text! == "123456"){
                        self.statusVerifyTextLabel.textColor = #colorLiteral(red: 0.3068847047, green: 0.7658930421, blue: 0.2954145416, alpha: 1)
                        self.statusVerifyTextLabel.text = "Varify Successed"
                        self.defaults.set("yes", forKey: "isVerifyied")
                        print("verify Successed")
                        
                        DispatchQueue.main.async {
                            print("self.vtoken: \(self.vtoken)")
                            self.readyForToken(token: self.vtoken)
                        }
                    }else{
                        print("error to verify otp code")
                        self.statusVerifyTextLabel.textColor = #colorLiteral(red: 0.9254902005, green: 0.2352941185, blue: 0.1019607857, alpha: 1)
                        self.statusTextChanged(text: "Invalid OTP code")
                        self.removeSpinner()
                    }
                }else{
                    let credential: PhoneAuthCredential = PhoneAuthProvider.provider().credential(withVerificationID: defaults.string(forKey: "authVID")!, verificationCode: codeTextField.text!)
                    print("verifying")
                    Auth.auth().signIn(with: credential) { (user, error) in
                        
                        //if error to verify code
                        if error != nil {
                            print("error to verify otp code")
                            self.statusVerifyTextLabel.textColor = #colorLiteral(red: 0.9254902005, green: 0.2352941185, blue: 0.1019607857, alpha: 1)
                            self.statusTextChanged(text: "Invalid OTP code")
                            self.removeSpinner()
                            print("error: \(String(describing: error?.localizedDescription))")
                        }
                            //if successfully varified
                        else{
                            self.statusVerifyTextLabel.textColor = #colorLiteral(red: 0.3068847047, green: 0.7658930421, blue: 0.2954145416, alpha: 1)
                            self.statusVerifyTextLabel.text = "Varify Successed"
                            self.defaults.set("yes", forKey: "isVerifyied")
                            print("verify Successed")
                            
                            
                            let cUser = Auth.auth().currentUser
                            print("currentUser: \(cUser!)")
                            
                            DispatchQueue.main.async {
                                let currentUser = Auth.auth().currentUser
                                currentUser?.getIDTokenForcingRefresh(true) { idToken, error in
                                    if error != nil {
                                        // Handle error
                                        return;
                                    }else{
                                        // Send token to your backend via HTTPS
                                        // ...
                                        print("idtoken: \(idToken!)")
                                        self.vtoken = String(idToken!.prefix(152))
                                        print("self.vtoken: \(self.vtoken)")
                                        self.readyForToken(token: self.vtoken)
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }else{
            self.statusVerifyTextLabel.shakeLabel()
        }
        
    }
  
    func readyForToken(token: String){
        readyToken = token
        defaults.setValue(readyToken, forKey: "token")
        self.performSegue(withIdentifier: "logged", sender: Any?.self)
        self.removeSpinner()
        self.grayColor(onView: self.codeMainV)
    }
    
    
    //changing status label
    func statusTextChanged(text: String){
        if self.isInternetConnected {
            self.statusVerifyTextLabel.text = text
        }else{
            self.statusVerifyTextLabel.textColor = #colorLiteral(red: 0.9254902005, green: 0.2352941185, blue: 0.1019607857, alpha: 1)
            self.statusVerifyTextLabel.text = "No internet connection"
        }
        
    }
    
    
    //used for identifying internet connection state
    open func internetIdentifier(){
        reachability.whenReachable = { _ in
            DispatchQueue.main.async {
                //self.connectionStatus.text = "connected"
            }
        }
        
        reachability.whenUnreachable = { _ in
            DispatchQueue.main.async {
                //self.connectionStatus.text = "not connected"
            }
        }
        
        NotificationCenter.default.addObserver(self, selector: #selector(internetChanged), name: Notification.Name.reachabilityChanged, object: reachability)
        
        do{
            try reachability.startNotifier()
        }catch{
            print("could not start notifier")
        }
    }
    
    //used for when internet connection state changed
    @objc func internetChanged(note: Notification) {
        let reachability = note.object as! Reachability
        if reachability.connection != .none {
            print("internetChanged: connected")
            whenDeviceOnline()
            isInternetConnected = true
        }else{
            whenDeviceOffline()
            print("internetChanged: not connected")
            isInternetConnected = false
        }
    }
    
    //used for when device is online
    func whenDeviceOnline(){
        snackbar.backgroundColor = #colorLiteral(red: 0.3411764801, green: 0.6235294342, blue: 0.1686274558, alpha: 1)
        snackbar.messageTextAlign = .center
        snackbar.message = "Internet Connected"
        statusVerifyTextLabel.text = "Code sent to \(self.phoneNum)"
        statusVerifyTextLabel.textColor = #colorLiteral(red: 0.3068847047, green: 0.7658930421, blue: 0.2954145416, alpha: 1)
        DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + Double(Int64(3 * Double(NSEC_PER_SEC))) / Double(NSEC_PER_SEC)) {
            self.snackbar.dismiss()
        }
    }
    
    
    //used for when device is offline
    func whenDeviceOffline(){
        snackbar.backgroundColor = #colorLiteral(red: 0.2549019754, green: 0.2745098174, blue: 0.3019607961, alpha: 1)
        statusVerifyTextLabel.textColor = #colorLiteral(red: 0.9254902005, green: 0.2352941185, blue: 0.1019607857, alpha: 1)
        snackbar.messageTextAlign = .center
        statusVerifyTextLabel.text = "No internet connection"
        snackbar.message = "No Internet Connection"
        snackbar.show()
    }
    
    
    //start timer or count down
    func startTimer() {
        timer.invalidate()
        timer = Timer.scheduledTimer(timeInterval: 1.0, target: self, selector: #selector(updateTime), userInfo: nil, repeats: true)
    }
    
    //update time
    @objc func updateTime() {
        timeCounterLabel.text = "Resend code after \(timeFormatted(totalSecond)) sec"
        timeCounterLabel.textAlignment = .center
        if totalSecond != 0 {
            totalSecond -= 1
            //print(totalSecond)
        } else {
            resendBtnStyleWhenEnable()
            endTimer()
        }
    }
    
    //stop timer
    func endTimer() {
        totalSecond = 90
        timer.invalidate()
    }
    
    //time formatting
    func timeFormatted(_ totalSeconds: Int) -> String {
        let seconds: Int = totalSeconds % 60
        if totalSecond > 59 {
            return String(format: "01:%02d", seconds)
        }else{
             return String(format: "00:%02d", seconds)
        }
    }

}

