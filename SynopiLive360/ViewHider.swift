//
//  ViewHider.swift
//  SynopiLive360
//
//  Created by Syncrhonous on 23/5/19.
//  Copyright © 2019 Syncrhonous. All rights reserved.
//

import Foundation


class ViewHiderOrEnabler: NSObject {
    
    func disOrEnableMultipleButtons(buttons: [UIButton], dissAble: [Bool]) {
        for (index, button) in buttons.enumerated() {
            switch dissAble[index] {
            case true:
                button.isEnabled = true
                //button.alpha = 0.3
            case false:
                button.isEnabled = false
                //button.alpha = 1.0
            }
            
        }
    }



}
