//
//  SettingsViewController.swift
//  SynopiLive360
//
//  Created by Syncrhonous on 4/4/19.
//  Copyright © 2019 Syncrhonous. All rights reserved.
//

import UIKit

class SettingsViewController: UIViewController {
    
    var labelValue: String = ""
    @IBOutlet weak var camFacingView: UIView!
    @IBOutlet weak var videoBitrateV: UIView!
    @IBOutlet weak var videoFrameV: UIView!
    @IBOutlet weak var audioSampleV: UIView!
    @IBOutlet weak var audioBitV: UIView!
    @IBOutlet weak var recordV: UIView!
    @IBOutlet weak var channelV: UIView!
    
    
    @IBOutlet weak var camLabel: UILabel!
    @IBOutlet weak var vBitLabel: UILabel!
    @IBOutlet weak var vFrameLabel: UILabel!
    @IBOutlet weak var aSampleLabel: UILabel!
    @IBOutlet weak var aBitLabel: UILabel!
    @IBOutlet weak var recordingLabel: UILabel!
    
    let userPermit = UserPermission()
    let defaults = UserDefaults.standard
    let kbps: String = " kbps"
    let fps: String = " fps"
    let kHz: String = " kHz"
    var camData: String = ""
    var recording: String = "Disable"

    @IBOutlet var settingScrollView: UIView!
    @IBOutlet weak var settingScrollViewOneInnae: UIScrollView!
    @IBOutlet weak var settingsScrollTwoInnarView: UIView!
    
    @objc func refreshLbl(notification: NSNotification) {
        let cc = defaults.string(forKey: "vCamFace") ?? "Back Camera"
        camLabel.text = cc
        let record = defaults.string(forKey: "record") ?? "Disabled"
        recordingLabel.text = record
       
        print("Received Notification: \(cc)")
        //lbl.text = "LogOut"
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        NotificationCenter.default.addObserver(self,selector: #selector(refreshLbl),
                                               name: NSNotification.Name(rawValue: "refresh"),object: nil)
        NotificationCenter.default.addObserver(self,selector: #selector(refreshLbl),
                                               name: NSNotification.Name(rawValue: "recordings"),object: nil)
        settingScrollView.layer.cornerRadius = 8
        settingScrollViewOneInnae.layer.cornerRadius = 8
        settingsScrollTwoInnarView.layer.cornerRadius = 8
        print("viewDidLoad settings")
        camLabel.text = "Back Camera"
        //from local storage
        defaultDataSetting()
        //facing camera view clicked
       // let camFace = UITapGestureRecognizer(target: self, action: #selector(cameraFacingViewAction(_:)))
        //self.camFacingView.addGestureRecognizer(camFace)
        
        //video bitrate view clicked
        let vBit = UITapGestureRecognizer(target: self, action: #selector(videoBitViewAction(_:)))
        self.videoBitrateV.addGestureRecognizer(vBit)
        
        //video frame view clicked
        let vFrame = UITapGestureRecognizer(target: self, action: #selector(videoFrameViewAction(_:)))
        self.videoFrameV.addGestureRecognizer(vFrame)
        
        //audio bitrate view clicked
        let aBit = UITapGestureRecognizer(target: self, action: #selector(audioBitViewAction(_:)))
        self.audioBitV.addGestureRecognizer(aBit)
        
        //audio bitrate view clicked
        let recoV = UITapGestureRecognizer(target: self, action: #selector(recordViewAction(_:)))
        self.recordV.addGestureRecognizer(recoV)
        
        //audio sample view clicked
        let aSample = UITapGestureRecognizer(target: self, action: #selector(audioSampleViewAction(_:)))
        self.audioSampleV.addGestureRecognizer(aSample)
    }
    
    //from local storage
    func defaultDataSetting(){
        let cc = defaults.string(forKey: "vCamFace") ?? "Back Camera"
        let cvb = defaults.string(forKey: "vBitFace") ?? "1000"
        let avb = defaults.string(forKey: "aBitFace") ?? "128"
        let vff = defaults.string(forKey: "vFrameFace") ?? "30"
        let asf = defaults.string(forKey: "aSampleFace") ?? "44100"
        let record = defaults.string(forKey: "record") ?? "Disabled"
        
        print("cc: \(cc)")
        print("cvb: \(cvb)")
        print("avb: \(avb)")
        print("vff: \(vff)")
        print("asf: \(asf)")
        print("record: \(record)")
        
        let a = avb + kbps
        print("a: \(a)")
        
        camLabel.text = cc
        vBitLabel.text = cvb + kbps
        aBitLabel.text = avb + kbps
        vFrameLabel.text = vff + fps
        aSampleLabel.text = asf + kHz
        recordingLabel.text = record
    }
    
    
    
    
    //recording view clicked
    @objc func recordViewAction(_ sender:UITapGestureRecognizer){
        DispatchQueue.main.async {
            if self.userPermit.checkPhotoLibraryPermission(){
                return
            }
        }
        labelValue = recordingLabel.text!
        print("labelValue recordingLabel: \(recordingLabel!)")
        customRecordingAlert(recoding: labelValue)
    }
    
  
    //facing camera view clicked
    @objc func cameraFacingViewAction(_ sender:UITapGestureRecognizer){
        labelValue = camLabel.text!
        print("labelValue camera: \(labelValue)")
        customCamFaceAlert(camFace: labelValue)
    }
    
    //video bitrate view clicked
    @objc func videoBitViewAction(_ sender:UITapGestureRecognizer){
        //videoBitrateV.viewFlash()
        labelValue = vBitLabel.text!
        print("cam facing view clicked: \(labelValue)")
        let subS = String(labelValue.dropLast(5))
        customTextFieldAlert(head: "Video Bitrate", fValue: subS, lValue: "*Video Bitrate: 50 ~ 4000", identity: "videoBit", pHolder: "1000")
    }
    
    //video frame view clicked
    @objc func videoFrameViewAction(_ sender:UITapGestureRecognizer){
        labelValue = vFrameLabel.text!
        print("cam facing view clicked: \(labelValue)")
        let subS = String(labelValue.dropLast(4))
        customTextFieldAlert(head: "Video Framerate", fValue: subS, lValue: "*Video Framerate: 5 ~ 30", identity: "videoFrame", pHolder: "25")
    }
    
    //audio bitrate view clicked
    @objc func audioBitViewAction(_ sender:UITapGestureRecognizer){
        labelValue = aBitLabel.text!
        print("cam facing view clicked: \(labelValue)")
        let subS = String(labelValue.dropLast(5))
        customTextFieldAlert(head: "Audio Bitrate", fValue: subS, lValue: "*Audio Bitrate: 32 ~ 512", identity: "audioBit", pHolder: "128")
    }
    
    //audio sample view clicked
    @objc func audioSampleViewAction(_ sender:UITapGestureRecognizer){
        labelValue = aSampleLabel.text!
        print("cam facing view clicked: \(labelValue)")
        let subSs = String(labelValue.dropLast(4))
        customSampleAlert(sampleValue: subSs)
    }
    
    //used only these alert where textield available
    func customTextFieldAlert(head: String, fValue: String, lValue: String, identity: String, pHolder: String){
        let textFieldAlert = self.storyboard?.instantiateViewController(withIdentifier: "alertTextField") as! AlertCommonTextFieldViewController
        textFieldAlert.providesPresentationContextTransitionStyle = true
        textFieldAlert.definesPresentationContext = true
        textFieldAlert.modalPresentationStyle = UIModalPresentationStyle.overCurrentContext
        textFieldAlert.modalTransitionStyle = UIModalTransitionStyle.crossDissolve
        textFieldAlert.delegate = self
        textFieldAlert.setData(heading: head, fieldValue: fValue, limitValue: lValue, identifier: identity, placeHolder: pHolder)
        self.present(textFieldAlert, animated: true, completion: nil)
    }
    
    
    //recodring alert
    func customRecordingAlert(recoding: String){
        let rAlert = self.storyboard?.instantiateViewController(withIdentifier: "recordingViewId") as! AlertRecordingViewController
        rAlert.providesPresentationContextTransitionStyle = true
        rAlert.definesPresentationContext = true
        rAlert.modalPresentationStyle = UIModalPresentationStyle.overCurrentContext
        rAlert.modalTransitionStyle = UIModalTransitionStyle.crossDissolve
        rAlert.delegate = self
        rAlert.setData(recordValue: recoding)
        self.present(rAlert, animated: true, completion: nil)
    }
    
    //sample alert
    func customCamFaceAlert(camFace: String){
        let camAlert = self.storyboard?.instantiateViewController(withIdentifier: "alertCamFaceId") as! AlertCameraFacingViewController
        camAlert.providesPresentationContextTransitionStyle = true
        camAlert.definesPresentationContext = true
        camAlert.modalPresentationStyle = UIModalPresentationStyle.overCurrentContext
        camAlert.modalTransitionStyle = UIModalTransitionStyle.crossDissolve
        camAlert.delegate = self
        camAlert.setData(camFacing: camFace)
        self.present(camAlert, animated: true, completion: nil)
    }
    
    
    //sample alert
    func customSampleAlert(sampleValue: String){
        let sAlert = self.storyboard?.instantiateViewController(withIdentifier: "alertSampleId") as! AlertSamplerateViewController
        sAlert.providesPresentationContextTransitionStyle = true
        sAlert.definesPresentationContext = true
        sAlert.modalPresentationStyle = UIModalPresentationStyle.overCurrentContext
        sAlert.modalTransitionStyle = UIModalTransitionStyle.crossDissolve
        sAlert.delegate = self
        sAlert.setData(sampleValue: sampleValue)
        self.present(sAlert, animated: true, completion: nil)
    }
    
   
}

//setting label's data changed
extension SettingsViewController: AlertCommonTextFieldViewDelegate {
    
    //when clicking ok button
    func okButtonTapped(selectedOption: String, textFieldValue: String) {
        print("selectedOption: \(selectedOption)")
        //which ok button is clicked, is identified by selectedOption
        if selectedOption == "videoBitRateOk" {
            print("print")
            self.vBitLabel.text = textFieldValue + " kbps"
        }else if selectedOption == "audioBitRateOk" {
            print("print")
            self.aBitLabel.text = textFieldValue + " kbps"
        }else if selectedOption == "videoFrameRateOk" {
            print("print")
            self.vFrameLabel.text = textFieldValue + " fps"
        }else if selectedOption == "cameraOk" {
            print("print")
            self.camLabel.text = textFieldValue
        }else if selectedOption == "audioSampleRateOk" {
            print("print")
            self.aSampleLabel.text = textFieldValue + " kHz"
        }else if selectedOption == "recordingOk" {
            print("print")
            self.recordingLabel.text = textFieldValue
        }
    }
    
    //when clicking cancel button
    func cancelButtonTapped() {
        print("cancelButtonTapped")
    }
}
