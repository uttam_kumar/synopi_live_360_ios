//
//  AlertCameraFacingViewController.swift
//  SynopiLive360
//
//  Created by Syncrhonous on 6/4/19.
//  Copyright © 2019 Syncrhonous. All rights reserved.
//



//alert to check or make camera facing front or back

import UIKit

class AlertCameraFacingViewController: UIViewController {

    var delegate: AlertCommonTextFieldViewDelegate?
    
    @IBOutlet weak var frontCamera: DLRadioButton!
    @IBOutlet weak var backCamera: DLRadioButton!
    @IBOutlet weak var camFaceView: UIView!
    @IBOutlet weak var okBtn: UIButton!
    @IBOutlet weak var cancelBtn: UIButton!
    
    var camFacing: String = "Back Camera"
    var selectedCam: String = "Back Camera"
    let defaults = UserDefaults.standard
    
    //receiving data (like constructor)
    func setData(camFacing: String){
        self.camFacing = camFacing
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        self.hideKeyboardWhenTappedAround()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        camFaceView?.isHidden = false
        setupCamFaceView()
        animateView()
        selectedRadioBtn()
    }
    
    //ok button clicked to store update value
    @IBAction func camOkBtnAction(_ sender: UIButton) {
        delegate?.okButtonTapped(selectedOption: "cameraOk", textFieldValue: selectedCam)
        defaults.set(selectedCam, forKey: "vCamFace")
        camFaceView.viewScaleDismiss()
        DispatchQueue.main.asyncAfter(deadline: .now() + .milliseconds(450)) {
            self.dismiss(animated: false, completion: nil)
            print("dfhhrtrr")
        }
    }
    
    //cancel button clicked not to store update value
    @IBAction func camCancelBtnAction(_ sender: UIButton) {
        camFaceView.viewScaleDismiss()
        DispatchQueue.main.asyncAfter(deadline: .now() + .milliseconds(450)) {
            self.dismiss(animated: false, completion: nil)
            print("dfhhrtrr")
        }
    }
    
    
    //when selected on radio clicked
    @IBAction func selectionUpdateAction(_ sender: DLRadioButton) {
        selectedCam = sender.titleLabel!.text!
        print("selectedCam: \(selectedCam)")
    }
    
    //previous selected data (retrieve from local storage)
    func selectedRadioBtn(){
        if camFacing == "Back Camera" {
            backCamera.isSelected = true
        }else{
            frontCamera.isSelected = true
        }
    }
    
    
    //view set up
    func setupCamFaceView() {
        camFaceView.layer.cornerRadius = 8
        cancelBtn.layer.cornerRadius = 8
        okBtn.layer.cornerRadius = 8
        camFaceView.center = CGPoint(x: camFaceView.frame.size.width  / 2,
                                        y: camFaceView.frame.size.height / 2)
        self.view.backgroundColor = UIColor.black.withAlphaComponent(0.4)
    }
    
    
    //view animated
    func animateView() {
        camFaceView.viewScaleDisplay()
        camFaceView.center = view.center
    }
}



//extension for hiding keyboard on outside touch
extension AlertCameraFacingViewController {
    func hideKeyboardWhenTappedAround() {
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(AlertCommonTextFieldViewController.dismissKeyboard))
        tap.cancelsTouchesInView = false
        view.addGestureRecognizer(tap)
    }
    
    @objc func dismissKeyboard() {
        view.endEditing(true)
    }
}
