//
//  TestViewController.swift
//  SynopiLive360
//
//  Created by Syncrhonous on 25/4/19.
//  Copyright © 2019 Syncrhonous. All rights reserved.
//


//alert to check or make recording enabled or disabled
import UIKit

class AlertRecordingViewController: UIViewController {
    
    var delegate: AlertCommonTextFieldViewDelegate?
    let defaults = UserDefaults.standard
    var userPermit = UserPermission()
    
    
    @IBOutlet weak var vview: UIView!
    @IBOutlet weak var alertCancelBtn: UIButton!
    @IBOutlet weak var alertOkBtn: UIButton!
    @IBOutlet weak var enabledRadioBtn: DLRadioButton!
    @IBOutlet weak var disabledRadioBtn: DLRadioButton!
    
    
    var recordValue = "Disabled"
    var selectedRecording: String = "Disabled"
    
    //textfield data (like constructor)
    func setData(recordValue: String){
        self.recordValue = recordValue
    }
  
    override func viewDidLoad() {
        super.viewDidLoad()
        self.hideKeyboardWhenTappedAround()
        // Do any additional setup after loading the view.
    }
    
    
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        vview?.isHidden = false
        setupViewTextField()
        animateView()
        selectedRadioBtn()
    }
    
    
    //set up view
    func setupViewTextField() {
        vview.layer.cornerRadius = 8
        alertCancelBtn.layer.cornerRadius = 8
        alertOkBtn.layer.cornerRadius = 8
        vview.center = CGPoint(x: vview.frame.size.width  / 2, y: vview.frame.size.height / 2)
        self.view.backgroundColor = UIColor.black.withAlphaComponent(0.4)
    }
    
    
    //animated view
    func animateView() {
        vview.viewScaleDisplay()
        vview.center = view.center
    }
    
    //select radio button
    @IBAction func selectionUpdateAction(_ sender: DLRadioButton) {
        sender.shake()
        selectedRecording = sender.titleLabel!.text!
        print("selectedSample: \(selectedRecording)")
    }
    
    //cancel to store updated data
    @IBAction func cancelButtonClicked(_ sender: UIButton) {
        vview.viewScaleDismiss()
        DispatchQueue.main.asyncAfter(deadline: .now() + .milliseconds(450)) {
            self.dismiss(animated: false, completion: nil)
            print("dfhhrtrr")
        }
    }
    
    //save to store updated data
    @IBAction func okButtonClicked(_ sender: UIButton) {
        delegate?.okButtonTapped(selectedOption: "recordingOk", textFieldValue: selectedRecording)
        defaults.set(selectedRecording, forKey: "record")
        vview.viewScaleDismiss()
        DispatchQueue.main.asyncAfter(deadline: .now() + .milliseconds(450)) {
            self.dismiss(animated: false, completion: nil)
            print("dfhhrtrr")
        }
    }
    
    
    //previous selected data (retrieve from local storage)
    func selectedRadioBtn(){
        if recordValue == "Enabled" {
            enabledRadioBtn.isSelected = true
           
        }else if recordValue == "Disabled" {
            disabledRadioBtn.isSelected = true
        }
    }
    
}

//extension for hiding keyboard on outside touch
extension AlertRecordingViewController {
    func hideKeyboardWhenTappedAround() {
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(AlertCommonTextFieldViewController.dismissKeyboard))
        tap.cancelsTouchesInView = false
        view.addGestureRecognizer(tap)
    }
    
    @objc func dismissKeyboard() {
        view.endEditing(true)
    }
}
