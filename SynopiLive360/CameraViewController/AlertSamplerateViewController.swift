//
//  AlertSamplerateViewController.swift
//  SynopiLive360
//
//  Created by Syncrhonous on 6/4/19.
//  Copyright © 2019 Syncrhonous. All rights reserved.
//

import UIKit

class AlertSamplerateViewController: UIViewController {
    
    var delegate: AlertCommonTextFieldViewDelegate?

    @IBOutlet weak var s48000: DLRadioButton!
    @IBOutlet weak var s44100: DLRadioButton!
    @IBOutlet weak var s32000: DLRadioButton!
    @IBOutlet weak var s22050: DLRadioButton!
    @IBOutlet weak var s16000: DLRadioButton!
    @IBOutlet weak var s11025: DLRadioButton!
    @IBOutlet weak var sampleRateView: UIView!
    
    @IBOutlet weak var okBtn: UIButton!
    @IBOutlet weak var cancelBtn: UIButton!
    
    var sampleValue: String = "44100"
    var selectedSample: String = "44100"
    let defaults = UserDefaults.standard
    
    //receiving data
    func setData(sampleValue: String){
        self.sampleValue = sampleValue
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        self.hideKeyboardWhenTappedAround()
    }
    
    
    open func viewHidder(){
        self.dismiss(animated: false, completion: nil)
        //sampleRateView?.isHidden = true
    }
    
    override func viewWillAppear(_ animated: Bool) {
        sampleRateView?.isHidden = false
        setupSampleView()
        animateView()
        selectedRadioBtn()
    }
    
    //ok button clicked
    @IBAction func sampleOkBtnAction(_ sender: UIButton) {
        delegate?.okButtonTapped(selectedOption: "audioSampleRateOk", textFieldValue: selectedSample)
        defaults.set(selectedSample, forKey: "aSampleFace")
        sampleRateView.viewScaleDismiss()
        DispatchQueue.main.asyncAfter(deadline: .now() + .milliseconds(450)) {
            self.dismiss(animated: false, completion: nil)
            print("dfhhrtrr")
        }
    }
    
    //cancel button clicked
    @IBAction func sampleCancelBtnAction(_ sender: UIButton) {
        sampleRateView.viewScaleDismiss()
        DispatchQueue.main.asyncAfter(deadline: .now() + .milliseconds(450)) {
            self.dismiss(animated: false, completion: nil)
            print("dfhhrtrr")
        }
    }
    
    @IBAction func selectionUpsateAction(_ sender: DLRadioButton) {
        sender.shake()
        selectedSample = sender.titleLabel!.text!
        print("selectedSample: \(selectedSample)")
    }
    

    //previous selected data (retrieve from local storage)
    func selectedRadioBtn(){
        if sampleValue == "48000" {
            s48000.isSelected = true
        }else if sampleValue == "44100" {
            s44100.isSelected = true
        }else if sampleValue == "32000" {
            s32000.isSelected = true
        }else if sampleValue == "22050" {
            s22050.isSelected = true
        }else if sampleValue == "16000" {
            s16000.isSelected = true
        }else if sampleValue == "11025" {
            s11025.isSelected = true
        }
    }
    
    
    //set up view
    func setupSampleView() {
        sampleRateView.layer.cornerRadius = 8
        cancelBtn.layer.cornerRadius = 8
        okBtn.layer.cornerRadius = 8
        sampleRateView.center = CGPoint(x: sampleRateView.frame.size.width  / 2, y: sampleRateView.frame.size.height / 2)
        self.view.backgroundColor = UIColor.black.withAlphaComponent(0.4)
    }


    //animated view
    func animateView() {
        sampleRateView.viewScaleDisplay()
        sampleRateView.center = view.center
    }
    
}



//extension for hiding keyboard on outside touch
extension AlertSamplerateViewController {
    func hideKeyboardWhenTappedAround() {
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(AlertCommonTextFieldViewController.dismissKeyboard))
        tap.cancelsTouchesInView = false
        view.addGestureRecognizer(tap)
    }
    
    @objc func dismissKeyboard() {
        view.endEditing(true)
    }
}
