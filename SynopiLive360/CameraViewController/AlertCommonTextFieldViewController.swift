//
//  AlertCommonTextFieldViewController.swift
//  SynopiLive360
//
//  Created by Syncrhonous on 4/4/19.
//  Copyright © 2019 Syncrhonous. All rights reserved.
//

//alert dialog for audio bitrate, video bitrate, video framerate
import UIKit

class AlertCommonTextFieldViewController: UIViewController {
    let defaults = UserDefaults.standard
    var delegate: AlertCommonTextFieldViewDelegate?
    
    @IBOutlet weak var alertHeadingTitle: UILabel!
    @IBOutlet weak var alertValueTextField: UITextField!
    @IBOutlet weak var alertSuggestedValueLabel: UILabel!
    @IBOutlet weak var alertOkBtn: UIButton!
    @IBOutlet weak var alertCancelBtn: UIButton!
    @IBOutlet weak var alertView: UIView!
    @IBOutlet weak var textFieldEmptyBtn: UIButton!
    
    var heading: String = ""
    var fieldValue: String = ""
    var limitValue: String = ""
    var identifier: String = ""
    var placeHolder: String = ""
    var haveKeyboard: Bool = false
    var yAxis: CGFloat = 0.0
    var xAxis: CGFloat = 0.0
    //textfield data
    func setData(heading: String, fieldValue: String, limitValue: String, identifier: String, placeHolder: String){
        self.heading = heading
        self.fieldValue = fieldValue
        self.limitValue = limitValue
        self.identifier = identifier
        self.placeHolder = placeHolder
    }
    
    
    
    @IBAction func textFieldEmptyBtnClicked(_ sender: UIButton) {
        goUpper()
        alertValueTextField.text = ""
        textFieldEmptyBtn.isHidden = true
        self.alertValueTextField.becomeFirstResponder()
    }
    
    //orientation landscape right
    override var supportedInterfaceOrientations: UIInterfaceOrientationMask {
        return .landscapeRight
    }
    
    //auto rotation false
    override var shouldAutorotate: Bool {
        return false
    }
    
    override var preferredInterfaceOrientationForPresentation: UIInterfaceOrientation{
        return .landscapeRight
    }
    

    

    override func viewDidLoad() {
        super.viewDidLoad()
        //self.hideKeyboardWhenTappedAround()

        // alert view clicked
        let aView = UITapGestureRecognizer(target: self, action: #selector(viewClickAction(_:)))
        self.alertView.addGestureRecognizer(aView)
        // Do any additional setup after loading the view.
    }
    
    ///dont remove
    //video bitrate view clicked
    @objc func viewClickAction(_ sender:UITapGestureRecognizer){

    }
    
    
    //go up when keyboard is on display
    func goUpper() {
        self.alertView.frame.origin.y = self.alertView.frame.origin.y
        UIView.animate(withDuration: 0.4, animations: { () -> Void in
            self.alertView.frame.origin.y =  self.yAxis - 30
        })
    }
    
    //go down when keyboard is dismiss
    func goDown() {
        self.alertView.frame.origin.y = self.alertView.frame.origin.y
        UIView.animate(withDuration: 0.4, animations: { () -> Void in
            self.alertView.frame.origin.y = self.xAxis - 40
        })
    }
    
    @objc func myTargetFunction(){
        haveKeyboard = true
        goUpper()
    }
    

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        alertView?.isHidden = false
        setupViewTextField()
        animateView()
        paddingOfTextField()
        alertValueTextField.addTarget(self, action: #selector(myTargetFunction), for: .touchDown)
        func myTargetFunction(textField: UITextField) {
            print("myTargetFunction")
        }
        
        alertValueTextField.addTarget(self, action: #selector(textFieldDidChange(sender:)), for: .editingChanged)
    }
    
   
    
    @objc func textFieldDidChange(sender: UITextField) {
        let s:Int = sender.text!.count
       // print("counter: \(s)")
        if s == 0 {
            textFieldEmptyBtn.isHidden = true
        }else{
            textFieldEmptyBtn.isHidden = false
        }
    }
    
    
    //set up view
    func setupViewTextField() {
        alertHeadingTitle.text = heading
        alertValueTextField.text = fieldValue
        alertSuggestedValueLabel.text = limitValue
        alertSuggestedValueLabel.textColor = #colorLiteral(red: 0.6372012678, green: 0.6372012678, blue: 0.6372012678, alpha: 1)
        alertValueTextField.placeholder = placeHolder
        alertValueTextField.keyboardAppearance = .dark
        alertValueTextField.keyboardType = .numberPad
        alertView.layer.cornerRadius = 8
        alertCancelBtn.layer.cornerRadius = 8
        alertOkBtn.layer.cornerRadius = 8
        alertView.center = CGPoint(x: alertView.frame.size.width  / 2, y: alertView.frame.size.height / 2)
        self.view.backgroundColor = UIColor.black.withAlphaComponent(0.4)
        
        yAxis = alertView.frame.size.height  / 2
        xAxis = alertView.frame.size.width  / 2
    }
    

    //animated view
    func animateView() {
        alertView.viewScaleDisplay()
        alertView.center = view.center
    }
    
    //padding for textfield
    func paddingOfTextField(){
        alertValueTextField.leftView = UIView(frame: CGRect(x: 0, y: 0, width: 4, height: alertValueTextField.frame.height))
        alertValueTextField.leftViewMode = .always
    }
    
    
    //ok button clicked
    @IBAction func alertOkBtnClicked(_ sender: UIButton) {
        sender.shake()
        goDown()
        alertValueTextField.resignFirstResponder()
        let textFieldVal: String = alertValueTextField.text!
 
        var sOption: String = ""
        var fKey: String = ""

        if identifier == "videoBit" {
            sOption = "videoBitRateOk"
            if videoBitrateValidation(textFieldVal: textFieldVal){
                fKey = "vBitFace"
            }else{
                fKey = ""
            }
            
        }else if identifier == "audioBit" {
            sOption = "audioBitRateOk"
            if audioBitrateValidation(textFieldVal: textFieldVal){
                fKey = "aBitFace"
            }else{
                fKey = ""
            }
        }else if identifier == "videoFrame" {
            sOption = "videoFrameRateOk"
            if videoFramerateValidation(textFieldVal: textFieldVal){
                fKey = "vFrameFace"
            }else{
                fKey = ""
            }
        }
        
        if fKey == ""{
            //print("invalid data")
            alertSuggestedValueLabel.textColor = #colorLiteral(red: 0.9254902005, green: 0.2352941185, blue: 0.1019607857, alpha: 1)
        }else{
            defaults.set(textFieldVal, forKey: fKey)
            delegate?.okButtonTapped(selectedOption: sOption, textFieldValue: alertValueTextField.text!)
            print(heading)
            print(fieldValue)
            print(limitValue)
            print(identifier)
            alertView.viewScaleDismiss()
            DispatchQueue.main.asyncAfter(deadline: .now() + .milliseconds(450)) {
                self.dismiss(animated: false, completion: nil)
                print("dfhhrtrr")
            }
        }
    }
    
    func videoBitrateValidation(textFieldVal: String) -> Bool{
        let inputValue = UInt32(textFieldVal) ?? 0
        let characterLength = textFieldVal.count
        if characterLength > 4 {
            return false
        }
        
        
        if(inputValue > 4000 || inputValue < 50){
            return false
        }else{
            return true
        }
    }
    
    func audioBitrateValidation(textFieldVal: String) -> Bool{
        let inputValue = UInt32(textFieldVal) ?? 0
        
        let characterLength = textFieldVal.count
        if characterLength > 3 {
            return false
        }
        
        if(inputValue > 512 || inputValue < 32){
            return false
        }else{
            return true
        }
    }
    
    func videoFramerateValidation(textFieldVal: String) -> Bool{
        let inputValue = Float64(textFieldVal) ?? 0
        
        let characterLength = textFieldVal.count
        if characterLength > 2 {
            return false
        }
        
        if(inputValue > 30 || inputValue < 5){
            return false
        }else{
            return true
        }
    }
    
    //cancel button clicked
    @IBAction func alertCancelBtnClicked(_ sender: UIButton) {
        alertValueTextField.resignFirstResponder()
        delegate?.cancelButtonTapped()
        //let sss = textFieldValueValidation(option: "bitrate", value: "1200")
        alertView.viewScaleDismiss()
        DispatchQueue.main.asyncAfter(deadline: .now() + .milliseconds(450)) {
            self.dismiss(animated: false, completion: nil)
            print("dfhhrtrr")
        }
        //self.dismiss(animated: true, completion: nil)
    }

    open func viewHidder(){
        self.dismiss(animated: false, completion: nil)
    }
    
    
    override func viewDidDisappear(_ animated: Bool) {
        print("view dis")
    }
    
}

//extension for hiding keyboard on outside touch
extension AlertCommonTextFieldViewController {
    func hideKeyboardWhenTappedAround() {
        goDown()
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(AlertCommonTextFieldViewController.dismissKeyboard))
        tap.cancelsTouchesInView = false
        view.addGestureRecognizer(tap)
    }
    
    @objc func dismissKeyboard() {
        if haveKeyboard {
            goDown()
        }
        haveKeyboard = false
        view.endEditing(true)
    }
}
