//
//  ViewController.swift
//  SynopiLive360
//
//  Created by Syncrhonous on 3/4/19.
//  Copyright © 2019 Syncrhonous. All rights reserved.
//

import AVFoundation
import HaishinKit
import Photos
import UIKit
import VideoToolbox
import Logboard
import TTGSnackbar

var streamAudioSample: Double = 44100
var streamVideoBit: UInt32 = 1000
var streamAudioBit: UInt32 = 128
var streamVideoFrame: Float64 = 30.0

//recording delegate
class ExampleRecorderDelegate: DefaultAVRecorderDelegate {
    override func didFinishWriting(_ recorder: AVRecorder) {
        
        guard let writer: AVAssetWriter = recorder.writer else {
            return
        }
        
        PHPhotoLibrary.shared().performChanges({() -> Void in
            PHAssetChangeRequest.creationRequestForAssetFromVideo(atFileURL: writer.outputURL)
        }, completionHandler: { _, error -> Void in
            do {
                try FileManager.default.removeItem(at: writer.outputURL)
            } catch {
                print(error)
            }
        })
    }
}


//main class
class LiveViewController: UIViewController {
    let defaultdata: DefaultStoreData = DefaultStoreData()
    var rtmpConnection = RTMPConnection()
    var rtmpStream: RTMPStream!
    var sharedObject: RTMPSharedObject!
    let diskInfo = DiskInformation()
    var userPermit = UserPermission()
    var timer = Timer()
    let sTimeCounter = StreamTimeCounter()
    let viewHiderOrEnabler: ViewHiderOrEnablerStyler = ViewHiderOrEnablerStyler()
    var settingsViewController: SettingsViewController = SettingsViewController()
    let reachability = Reachability()!
    let snackbar = TTGSnackbar(message: "TTGSnackbar !", duration: .forever)
    let recorderDelegate = ExampleRecorderDelegate()
    var toster: CustomRNToast = CustomRNToast()
    
     var goNextOnHandler: Bool = true
    var isStreaming = false
    var wasAppBackground = false
    var timeCounter: UInt32 = 0
    var isMuted: Bool = false
    
    var isInternetConnected = true
    var isAnimatedStreamButton: Bool = false
    
    var currentPosition: AVCaptureDevice.Position = .back
    let defaults = UserDefaults.standard
    
    @IBOutlet weak var lfView: GLHKView!
    @IBOutlet weak var settingsView: UIView!
    @IBOutlet weak var settingsTitleView: UIView!
    
    @IBOutlet weak var settingsBtn: UIButton!
    @IBOutlet weak var flashBtn: UIButton!
    @IBOutlet weak var cameraRotateBtn: UIButton!
    @IBOutlet weak var streamBtn: UIButton!
    @IBOutlet weak var audioMuteBtn: UIButton!
    @IBOutlet weak var streamTimeimeCount: UILabel!
    @IBOutlet weak var redCircle: UIImageView!
    @IBOutlet weak var recordIcon: UIImageView!

    @IBOutlet weak var s_settingsView: UIView!
    @IBOutlet weak var s_settingsViewC: UIView!
    
    //used as to rcv data from local storage
    var usableCamera: String = "Back Camera"
    var usableVideoBit: String = "1000"
    var usableVideoFrame: String = "30"
    var usableAudioBit: String = "128"
    var usableAudioSample: String = "44100"
    var recordStatus : String = "Disabled"
    var publishUrl: String = ""
    var publishKey: String = ""
    
    
    //var stackDataObj: DataModel?
    //audio mute action
    @IBAction func audioMuteAction(_ sender: UIButton) {
        audioMuteBtn.shake()
        print("audioooo: ")
        if audioMuteBtn.currentBackgroundImage == UIImage(named: "muted") {
            print("muted on")
            audioMuteBtn.setBackgroundImage(UIImage(named: "unmuted"), for: .normal)
            DispatchQueue.main.async {
                self.toster.showShortToast("warning", toastMessage: "Mic is unmuted.")
            }
            isMuted = false
        }else if audioMuteBtn.currentBackgroundImage == UIImage(named: "unmuted") {
            print("muted off")
            audioMuteBtn.setBackgroundImage(UIImage(named: "muted"), for: .normal)
            DispatchQueue.main.async {
                self.toster.showShortToast("warning", toastMessage: "Mic is muted.")
            }
            isMuted = true
        }
        rtmpStream?.audioSettings["muted"] = isMuted
    }
    
    

    
    //zoom slider
//    @IBOutlet weak var zoomSlider: UISlider!{
//        didSet{
//            zoomSlider.transform = CGAffineTransform(rotationAngle: CGFloat(-Double.pi / 2))
//        }
//    }
//
//    @IBAction func slidingZoomSlider(_ sender: UISlider) {
//        rtmpStream.setZoomFactor(CGFloat(sender.value), ramping: true, withRate: 5.0)
//    }
//
    
    
    
    //clicking settings button
    @IBAction func settingsBtnAction(_ sender: UIButton) {
        //sender.shake()
        settingsBtn.shake()
        if settingsView.isHidden {
            NotificationCenter.default.post(name: NSNotification.Name(rawValue: "refresh"), object: nil)
            NotificationCenter.default.post(name: NSNotification.Name(rawValue: "recordings"), object: nil)
            settingsView.isHidden = false
            settingsBtn.isEnabled = false
            if isInternetConnected {
                streamBtn.streamBtnDismiss()
            }else{
                streamBtn.isHidden = true
            }
            DispatchQueue.main.asyncAfter(deadline: .now() + .milliseconds(500)) {
                self.streamBtn.isHidden = true
                //self.zoomSlider.isHidden = true
                self.settingsBtn.isEnabled = true
            }
          
            settingsView.viewScaleDisplay()
            viewHiderOrEnabler.disOrEnableMultipleButtons(buttons: [flashBtn, cameraRotateBtn, audioMuteBtn], dissAble: [false, false, false])
        }else{
            settingsBtn.isEnabled = false
            streamBtn.isHidden = false
            //zoomSlider.isHidden = false
            if isInternetConnected {
                streamBtn.streamBtnDisplay()
                //zoomSlider.sliderScaleDisplay()
            }else{
                streamBtn.isHidden = true
                //zoomSlider.isHidden = true
            }
            //zoomSlider.value = 0.0
            settingsView.viewScaleDismiss()
            DispatchQueue.main.asyncAfter(deadline: .now() + .milliseconds(550)) {
                self.settingsView.isHidden = true
                self.settingsBtn.isEnabled = true
                print("dfhhrtrr")
            }
            viewHiderOrEnabler.disOrEnableMultipleButtons(buttons: [cameraRotateBtn, audioMuteBtn], dissAble: [true,true])
        
            if currentPosition.rawValue == 1 {
                flashBtn.isEnabled = true
            }
            defaultDataStore()
            prepareForStreamming()
        }
    }
    
    //clicking camera flash button
    @IBAction func flashBtnAction(_ sender: UIButton) {
        sender.shake()
        if !userPermit.checkMicAndCameraPermissionGranted(){
            userPermit.presentPhoneSettings(viewController: self, per: userPermit.micAndCameraPermissionChecker())
        }else{
            print("toggleTorch")
            if flashBtn.currentBackgroundImage == UIImage(named: "flash_on") {
                print("flash on")
                flashBtn.setBackgroundImage(UIImage(named: "flash-off"), for: .normal)
            }else if flashBtn.currentBackgroundImage == UIImage(named: "flash-off") {
                print("flash off")
                flashBtn.setBackgroundImage(UIImage(named: "flash_on"), for: .normal)
            }
            rtmpStream.torch = !rtmpStream.torch
        }
    }
    
    //clicking camera changed button
    @IBAction func cameraChangeBtnAction(_ sender: UIButton) {
        sender.shake()
        if !userPermit.checkMicAndCameraPermissionGranted(){
            userPermit.presentPhoneSettings(viewController: self, per: userPermit.micAndCameraPermissionChecker())
        }else{
            print("First: rotateCamera")
            let position: AVCaptureDevice.Position = currentPosition == .back ? .front : .back
            rtmpStream.attachCamera(DeviceUtil.device(withPosition: position)) { error in
                print(error.description)
            }
            currentPosition = position
            print("currentPosition: \(currentPosition.rawValue)")
            //currentoisition = 1 for back camera
            //currentposition = 2 for front camera
            rtmpStream.torch = false
            flashBtn.setBackgroundImage(UIImage(named: "flash_on"), for: .normal)
            if currentPosition.rawValue == 1 {
                defaults.set("Back Camera", forKey: "vCamFace")
                flashBtn.isEnabled = true
            }else if currentPosition.rawValue == 2 {
                flashBtn.isEnabled = false
                defaults.set("Front Camera", forKey: "vCamFace")
            }
            NotificationCenter.default.post(name: NSNotification.Name(rawValue: "refresh"), object: nil)
        }
    }
    
    
    //when clicked setting icon
    func cameraControlFromSettings(){
        if usableCamera == "Front Camera" {
            rtmpStream?.attachCamera(DeviceUtil.device(withPosition: .front))
        }else if usableCamera == "Back Camera" {
            rtmpStream?.attachCamera(DeviceUtil.device(withPosition: .back))
        }
    }
    

    //clicking stream publishing button
    @IBAction func publishBtnAction(_ sender: UIButton) {
        print("publish")
        if userPermit.checkMicAndCameraPermissionGranted(){
            redCircle?.isHidden = true
            recordIcon?.isHidden = true
            streamTimeimeCount?.isHidden = true
            streamTimeimeCount?.text = "00:00:00"
            timeCounter = 0
            if streamBtn.currentBackgroundImage == UIImage(named: "synopi_live_video_camera_red") {
                streamBtn.setBackgroundImage(UIImage(named: "synopi_live_video_camera_white"), for: .normal)
                settingsBtn.isEnabled = true
                isStreaming = false
                UIApplication.shared.isIdleTimerDisabled = false
                rtmpConnection.close()
                rtmpConnection.removeEventListener(Event.RTMP_STATUS, selector: #selector(rtmpStatusHandler), observer: self)
                print("disconnected")
            }else{
                if recordStatus == "Enabled" {
                    if userPermit.checkAllPermissionGranted(){
                        UIApplication.shared.isIdleTimerDisabled = true
                        rtmpConnection.addEventListener(Event.RTMP_STATUS, selector: #selector(rtmpStatusHandler), observer: self)
                        rtmpConnection.connect(publishUrl)
                        
                        sender.isUserInteractionEnabled = false
                        print("hlw 1")
                        DispatchQueue.main.asyncAfter(deadline: .now() + 2.0) {
                            sender.isUserInteractionEnabled = true
                        }
                    }else{
                         userPermit.presentPhoneSettings(viewController: self, per: userPermit.allPermissionChecker())
                    }
                }else{
                    UIApplication.shared.isIdleTimerDisabled = true
                    rtmpConnection.addEventListener(Event.RTMP_STATUS, selector: #selector(rtmpStatusHandler), observer: self)
                    rtmpConnection.connect(publishUrl)
                
                    sender.isUserInteractionEnabled = false
                    print("hlw 1")
                    DispatchQueue.main.asyncAfter(deadline: .now() + 2.0) {
                        sender.isUserInteractionEnabled = true
                    }
                }
            }
        }else{
            if recordStatus == "Enabled" {
                userPermit.presentPhoneSettings(viewController: self, per: userPermit.allPermissionChecker())
            }else{
                userPermit.presentPhoneSettings(viewController: self, per: userPermit.micAndCameraPermissionChecker())
            }
        }
    }
    
    
    //when app goes background
    open func whenAppGoesBackground(){
        print("App moved to background!")
        isAnimatedStreamButton = false
        timeCounter = 0
        wasAppBackground = true
        redCircle?.isHidden = true
        recordIcon?.isHidden = true
        settingsBtn?.isEnabled  = true
        streamTimeimeCount?.isHidden = true
        streamTimeimeCount?.text = "00:00:00"
        print("iosRT: false")
        settingsView?.isHidden = true
        streamBtn?.setBackgroundImage(UIImage(named: "synopi_live_video_camera_white"), for: .normal)
        isStreaming = false
        UIApplication.shared.isIdleTimerDisabled = false
        rtmpStream?.close()
        rtmpConnection.close()
        rtmpConnection.removeEventListener(Event.RTMP_STATUS, selector: #selector(rtmpStatusHandler), observer: self)
    }
    
    
    //when app goes foreground
    open func whenAppGoesForeground(){
        internetIdentifier()
        if (isInternetConnected){
            streamBtn?.isHidden = false
        }else{
            streamBtn?.isHidden = true
        }
        redCircle?.isHidden = true
        recordIcon?.isHidden = true
        settingsBtn?.isEnabled  = true
        streamTimeimeCount?.isHidden = true
        streamTimeimeCount?.text = "00:00:00"
        streamBtn?.setBackgroundImage(UIImage(named: "synopi_live_video_camera_white"), for: .normal)
        isStreaming = false
        print("App moved to foreground!")
        settingsBtn?.isEnabled  = true
        print("iosRT: false")
        settingsView?.isHidden = true
        cameraRotateBtn?.isEnabled = true
        usableCamera = defaults.string(forKey: "vCamFace") ?? usableCamera
        if (usableCamera == "Back Camera"){
            flashBtn?.isEnabled = true
        }else {
            flashBtn?.isEnabled = false
        }
    }
    

    override func viewDidLoad() {
        super.viewDidLoad()
        self.defaults.set("LiveViewController", forKey: "currentViewController")
        internetIdentifier()
        print("viewDidLoad liveviewcontroller")
        defaultDataStore()
        //corner radius 8 for settings view
        viewHiderOrEnabler.viewCornerRadius(views: [settingsView,settingsTitleView,s_settingsView,s_settingsViewC])
        prepareForStreamming()
    }
    

    //prepare for streamming // push data to stream
    func prepareForStreamming(){
        rtmpStream = RTMPStream(connection: rtmpConnection)
        //fixed orientation
        rtmpStream.syncOrientation = false
        rtmpStream.orientation = AVCaptureVideoOrientation.landscapeRight
        
        rtmpStream.captureSettings = [
            "sessionPreset": AVCaptureSession.Preset.hd1280x720.rawValue,
            "continuousAutofocus": false,
            "continuousExposure": false
        ]
        //if remove this, back camera making lag
        rtmpStream.captureSettings["preferredVideoStabilizationMode"] = AVCaptureVideoStabilizationMode.off.rawValue
        
        rtmpStream.videoSettings["width"] = 1280
        rtmpStream.videoSettings["height"] = 720
        rtmpStream.audioSettings["sampleRate"] = streamAudioSample
        rtmpStream.audioSettings["muted"] = isMuted
        rtmpStream.captureSettings["fps"] = streamVideoFrame
        rtmpStream.videoSettings["bitrate"] = streamVideoBit * 1024
        rtmpStream.audioSettings["bitrate"] = streamAudioBit * 1024

        rtmpStream.attachAudio(AVCaptureDevice.default(for: .audio)) { error in
            logger.warn(error.description)
            self.simpleErrorAlertDialog(title: "Error", msg: "Unable to attach Audio. Need Microphone permission")
            print("First: viewWillAppear audio error")
            print(error.description)
        }
        
        let camPosition = defaults.string(forKey: "vCamFace") ?? "Back Camera"
        if camPosition == "Back Camera"{
            currentPosition = .back
        }else{
            currentPosition = .front
        }
        
        rtmpStream.attachCamera(DeviceUtil.device(withPosition: currentPosition)) { error in
            self.simpleErrorAlertDialog(title: "Error", msg: "Unable to attach Camera. Need Camera permission.")
            print("First: viewWillAppear camera error")
            print(error.description)
        }
        
        rtmpStream.addObserver(self, forKeyPath: "currentFPS", options: .new, context: nil)
        lfView?.attachStream(rtmpStream)
        rtmpStream.mixer.recorder.delegate = recorderDelegate
    }
    
    
    //ready for streamming
    func streamValueSettingUp(){
        streamAudioSample = Double(usableAudioSample)!
        streamAudioBit = UInt32(usableAudioBit)!
        streamVideoBit = UInt32(usableVideoBit)!
        streamVideoFrame = Float64(usableVideoFrame)!
    }
    
    //supporting landscape right screen orientation
    override var supportedInterfaceOrientations: UIInterfaceOrientationMask {
        return .landscapeRight
    }

    //default data stores in local storage
    func defaultDataStore(){
        usableCamera = defaults.string(forKey: "vCamFace") ?? usableCamera
        usableVideoBit = defaults.string(forKey: "vBitFace") ?? usableVideoBit
        usableAudioBit = defaults.string(forKey: "aBitFace") ?? usableAudioBit
        usableVideoFrame = defaults.string(forKey: "vFrameFace") ?? usableVideoFrame
        usableAudioSample = defaults.string(forKey: "aSampleFace") ?? usableAudioSample
        recordStatus = defaults.string(forKey: "record") ?? recordStatus
        publishUrl = defaults.string(forKey: "publishUrl") ?? publishUrl
        publishKey = defaults.string(forKey: "publishKey") ?? publishKey
        
        defaultdata.dataStoreOnDefaultStorage(datas: [usableCamera, usableVideoBit, usableAudioBit, usableVideoFrame, usableAudioSample, recordStatus], keys: ["vCamFace","vBitFace","aBitFace","vFrameFace","aSampleFace","record",])
        
        streamValueSettingUp()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        internetIdentifier()
        print("viewWillAppear liveviewcontroller")
        super.viewWillAppear(animated)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        print("viewWillDisappear liveviewcontroller")
        super.viewWillDisappear(animated)
        rtmpStream.removeObserver(self, forKeyPath: "currentFPS")
        rtmpStream.close()
        rtmpStream.dispose()
    }
    
    //uttam-mobile 2
    //4P3kzslymvAluuX-QOcRteLVxqLPCibY34536pKNRJ4
    //ios-6
    //38sVUdx98uM9HUb_o5k5PQ
    //ios_6s / ipad
    //j7bb0rGuTZr-8XNkjKfbmg
    @objc
    func rtmpStatusHandler(_ notification: Notification) {
        if rtmpConnection.connected {
            print("recordStatus: \(recordStatus)")
            if recordStatus == "Enabled" {
                if userPermit.checkAllPermissionGranted(){
                    if diskInfo.haveFreeSpace(){
                        rtmpStream!.publish(publishKey, type: .localRecord)
                         goNextOnHandler = true
                    }else{
                        simpleAlertDialog(title: "Attention", msg: "Recording is disabled due to insufficient storage.")
                        recordStatus = "Disabled"
                        defaults.set(recordStatus, forKey: "record")
                         goNextOnHandler = false
                    }
                   
                }else{
                    userPermit.presentPhoneSettings(viewController: self, per: userPermit.allPermissionChecker())
                    goNextOnHandler = false
                }
            }else{
                goNextOnHandler = true
                rtmpStream!.publish(publishKey)
            }
            
            if goNextOnHandler{
                print("First: rtmpStatusHandler")
                let e = Event.from(notification)
                if let data: ASObject = e.data as? ASObject, let code: String = data["code"] as? String {
                    
                    print("code: \(code)")
                    switch code {
                    case "NetStream.Publish.Start":
                        print("iosRT: start publishing")
                        isStreaming = true
                        ifCanPublish(val: true)
                        break
                    case "NetStream.Unpublish.Success":
                        dataSettingsWhenCanNOtStream(msg: "")
                        print("iosRT: start unpublishing")
                        break
                    case "NetStream.Connect.Failed":
                        dataSettingsWhenCanNOtStream(msg: "")
                        print("iosRT: connect failed")
                        break
                    case "NetStream.Connect.Closed":
                        dataSettingsWhenCanNOtStream(msg: "")
                        print("iosRT: connect closed")
                        break
                    case "NetStream.Connect.Rejected":
                        dataSettingsWhenCanNOtStream(msg: "")
                        print("iosRT: connect rejected")
                        break
                    case "NetStream.Connect.Success":
                        print("iosRT: connect success")
                        break
                    case "NetStream.Publish.BadName":
                        dataSettingsWhenCanNOtStream(msg: "")
                        print("iosRT: this stream key is used for another streamming")
                        break
                    case "NetStream.Publish.Idle":
                        print("iosRT: publish idle")
                        break
                    //recording part
                    case "NetStream.Record.AlreadyExists":
                        dataSettingsWhenCanNOtStream(msg: "Record")
                        print("iosRT: NetStream.Record.AlreadyExists")
                        break
                    case "NetStream.Record.Failed":
                        dataSettingsWhenCanNOtStream(msg: "Record")
                        print("iosRT: NetStream.Record.Failed")
                        break
                    case "NetStream.Record.NoAccess":
                        dataSettingsWhenCanNOtStream(msg: "Record")
                        print("iosRT: NetStream.Record.NoAccess")
                        break
                    case "NetStream.Record.Start":
                        print("iosRT: NetStream.Record.Start")
                        break
                    case "NetStream.Record.Stop":
                        dataSettingsWhenCanNOtStream(msg: "Record")
                        print("iosRT: NetStream.Record.Stop")
                        break
                    case "NetStream.Record.DiskQuotaExceeded":
                        simpleAlertDialog(title: "Attention", msg: "Recording is disabled due to insufficient storage")
                        recordStatus = "Disabled"
                        defaults.set(recordStatus, forKey: "record")
                        print("iosRT: NetStream.Record.DiskQuotaExceeded")
                        break
                    default:
                        print("iosRT:")
                        break
                    }
                }
            }
        }else{
            if goNextOnHandler {
                dataSettingsWhenCanNOtStream(msg: "")
            }
            print("iosRT: could not connect, rtmpconnection")
        }
    
    }
    
    func dataSettingsWhenCanNOtStream(msg: String){
        var errorMsg: String = ""
        if msg == "" {
            errorMsg = "Something went wrong. Please try again."
        }else{
            errorMsg = "Couldn't record video. Please try again."
        }
        redCircle?.isHidden = true
        recordIcon?.isHidden = true
        streamTimeimeCount?.isHidden = true
        streamTimeimeCount?.text = "00:00:00"
        isStreaming = false
        ifCanPublish(val: false)
        DispatchQueue.main.async {
            //code
            self.toster.showLongToast("warning", toastMessage: errorMsg)
        }
    }
    
    
    func ifCanPublish(val: Bool){
        print("gbf: \(rtmpConnection.connected)")
        if val {
            print("iosRT: true")
            settingsBtn.isEnabled  = false
            streamBtn.setBackgroundImage(UIImage(named: "synopi_live_video_camera_red"), for: .normal)
            isStreaming = true
            
        }else{
            settingsBtn.isEnabled  = true
            print("iosRT: false")
            streamBtn.setBackgroundImage(UIImage(named: "synopi_live_video_camera_white"), for: .normal)
            isStreaming = false
            UIApplication.shared.isIdleTimerDisabled = false
            rtmpConnection.close()
            rtmpConnection.removeEventListener(Event.RTMP_STATUS, selector: #selector(rtmpStatusHandler), observer: self)
        }
    }
    
    func tapScreen(_ gesture: UIGestureRecognizer) {
        print("First: tapScreen")
        if let gestureView = gesture.view, gesture.state == .ended {
            let touchPoint: CGPoint = gesture.location(in: gestureView)
            let pointOfInterest = CGPoint(x: touchPoint.x / gestureView.bounds.size.width, y: touchPoint.y / gestureView.bounds.size.height)
            print("pointOfInterest: \(pointOfInterest)")
            rtmpStream.setPointOfInterest(pointOfInterest, exposure: pointOfInterest)
        }
    }
    
    
    override func observeValue(forKeyPath keyPath: String?, of object: Any?, change: [NSKeyValueChangeKey: Any]?, context: UnsafeMutableRawPointer?) {
        print("First: observeValueffff")
        if isStreaming {
            streamTimeimeCount?.isHidden = false
            timeCounter = timeCounter + 1
            let seconds = timeCounter % 60
            
            sTimeCounter.setData(timeCounter: timeCounter)
            
            if seconds % 2 != 0 {
                redCircle?.isHidden = false
                if recordStatus == "Enabled"{
                    recordIcon?.isHidden = false
                }else{
                    recordIcon?.isHidden = true
                }
            }else {
                redCircle?.isHidden = true
                recordIcon?.isHidden = true
            }
            streamTimeimeCount?.text = sTimeCounter.counting()//String("\(hour):\(min):\(sec)")
            if Thread.isMainThread {
                //currentFPSLabel?.text = "\(rtmpStream.currentFPS)"
            }
        }else{
            redCircle?.isHidden = true
            recordIcon?.isHidden = true
            streamTimeimeCount?.isHidden = true
            streamTimeimeCount?.text = "00:00:00"
        }
    }
    
    
    //used for identifying internet connection state
    open func internetIdentifier(){
        reachability.whenReachable = { _ in
            DispatchQueue.main.async {
                //self.connectionStatus.text = "connected"
            }
        }
        
        reachability.whenUnreachable = { _ in
            DispatchQueue.main.async {
                //self.connectionStatus.text = "not connected"
            }
        }
        
        NotificationCenter.default.addObserver(self, selector: #selector(internetChanged), name: Notification.Name.reachabilityChanged, object: reachability)
        
        do{
            try reachability.startNotifier()
        }catch{
            print("could not start notifier")
        }
    }
    
    
    //used for when internet connection state changed
    @objc func internetChanged(note: Notification) {
        let reachability = note.object as! Reachability
        if reachability.connection != .none {
            //self.connectionStatus.text = "connected"
            whenDeviceOnline()
            isInternetConnected = true
        }else{
            whenDeviceOffline()
            isInternetConnected = false
            //self.connectionStatus.text = "not connected"
        }
    }

    
    //used for when device is online
    open func whenDeviceOnline(){
        snackbar.backgroundColor = #colorLiteral(red: 0.3411764801, green: 0.6235294342, blue: 0.1686274558, alpha: 1)
        snackbar.messageTextAlign = .center
        snackbar.message = "Internet Connected"
        DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + Double(Int64(3 * Double(NSEC_PER_SEC))) / Double(NSEC_PER_SEC)) {
            self.snackbar.dismiss()
            if self.settingsView.isHidden{
                self.streamBtn.isHidden = false
                //self.zoomSlider.isHidden = false
                if self.isAnimatedStreamButton {
                    self.streamBtn.streamBtnDisplay()
                }
                //self.zoomSlider.sliderScaleDisplay()
            }else{
                self.streamBtn.isHidden = true
                //self.zoomSlider.isHidden = true
            }
        }
    }
    
    //used for when device is offline
    open func whenDeviceOffline(){
        isAnimatedStreamButton = true
        streamBtn.setBackgroundImage(UIImage(named: "synopi_live_video_camera_white"), for: .normal)
        streamTimeimeCount?.isHidden = true
        redCircle?.isHidden = true
        recordIcon?.isHidden = true
        streamTimeimeCount?.text = "00:00:00"
        ifCanPublish(val: false)
        //startRecording(recording: false)
        streamBtn.streamBtnDismiss()
        //zoomSlider.sliderScaleDismiss()
        DispatchQueue.main.asyncAfter(deadline: .now() + .milliseconds(500)) {
            self.streamBtn.isHidden = true
            //self.zoomSlider.isHidden = true
            print("dfhhrtrr")
        }
        isStreaming = false
        snackbar.backgroundColor = #colorLiteral(red: 0.2549019754, green: 0.2745098174, blue: 0.3019607961, alpha: 1)
        snackbar.messageTextAlign = .center
        snackbar.message = "No Internet Connection"
        snackbar.show()
    }

    //simple alert dialog for recording disable
    func simpleAlertDialog(title: String, msg: String){
        let alert = UIAlertController(title: title, message: msg, preferredStyle: UIAlertController.Style.alert)
        alert.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.default, handler:{ action in
            print("ok clicked")
            self.isStreaming = false
            self.recordStatus = "Disabled"
            self.defaults.set(self.recordStatus, forKey: "record")
            UIApplication.shared.isIdleTimerDisabled = false
            self.rtmpStream?.close()
            self.rtmpConnection.close()
            self.rtmpConnection.removeEventListener(Event.RTMP_STATUS, selector: #selector(self.rtmpStatusHandler), observer: self)
        }))
        
        // show the alert
        self.present(alert, animated: true, completion: nil)
    }
 
    
    //simple alert dialog for displaying error
    func simpleErrorAlertDialog(title: String, msg: String){
        let alert = UIAlertController(title: title, message: msg, preferredStyle: UIAlertController.Style.alert)
        alert.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.default, handler:{ action in
            // do something like...
            print("ok clicked")
            self.isStreaming = false
            UIApplication.shared.isIdleTimerDisabled = false
            self.rtmpStream?.close()
            self.rtmpConnection.close()
            self.rtmpConnection.removeEventListener(Event.RTMP_STATUS, selector: #selector(self.rtmpStatusHandler), observer: self)
        }))
        // show the alert
        self.present(alert, animated: true, completion: nil)
    }
}

