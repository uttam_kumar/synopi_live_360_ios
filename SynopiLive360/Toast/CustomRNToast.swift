//
//  CustomRNToast.swift
//  SynopiLive360
//
//  Created by Syncrhonous on 27/6/19.
//  Copyright © 2019 Syncrhonous. All rights reserved.
//


import Foundation
import UIKit

@objc(CustomRNToast)
class CustomRNToast : NSObject {
    //var toastContainer = UIView(frame: CGRect())
    @objc
    static func requiresMainQueueSetup()-> Bool {
        return true
    }
    
    @objc
    func showShortToast (_ toastType : String, toastMessage message : String)->Void {
        DispatchQueue.main.async{
            var presentViewController = UIApplication.shared.keyWindow?.rootViewController
            while let pVC = presentViewController?.presentedViewController
            {
                presentViewController = pVC
            }
            
            
            self.show(message: message, type : toastType, controller: presentViewController!, delay: 1.0)
            
        }
    }
    
    @objc
    func showLongToast (_ toastType : String, toastMessage message : String)->Void {
        DispatchQueue.main.async{
            var presentViewController = UIApplication.shared.keyWindow?.rootViewController
            while let pVC = presentViewController?.presentedViewController
            {
                presentViewController = pVC
            }
            
            self.show(message: message, type : toastType, controller: presentViewController!, delay: 3.0)
            
        }
    }
    
    @objc
    func hideToast()-> Void {
        DispatchQueue.main.async{
            var presentViewController = UIApplication.shared.keyWindow?.rootViewController
            while let pVC = presentViewController?.presentedViewController
            {
                presentViewController = pVC
            }
        }
        
    }
    
    func show(message: String, type : String, controller: UIViewController, delay: Double) {
        
        
        let toastContainer = UIView(frame: CGRect())
        
        if type == "danger" {
            toastContainer.backgroundColor = UIColor.red.withAlphaComponent(0.6)
        }else if type == "success" {
            toastContainer.backgroundColor = UIColor.green.withAlphaComponent(0.6)
        }else if type == "warning" {
            toastContainer.backgroundColor = #colorLiteral(red: 0.6799238037, green: 0.3529885309, blue: 0.7658930421, alpha: 1)
        }
        
        
        
        toastContainer.alpha = 0.0
        toastContainer.layer.cornerRadius = 8;
        toastContainer.clipsToBounds  =  true
        
        let toastLabel = UILabel(frame: CGRect())
        toastLabel.textColor = UIColor.white
        toastLabel.textAlignment = .center;
        toastLabel.font.withSize(15.0)
        toastLabel.text = message
        toastLabel.clipsToBounds  =  true
        toastLabel.numberOfLines = 0
        
        toastContainer.addSubview(toastLabel)
        controller.view.addSubview(toastContainer)
        
        toastLabel.translatesAutoresizingMaskIntoConstraints = false
        toastContainer.translatesAutoresizingMaskIntoConstraints = false
        
        let a1 = NSLayoutConstraint(item: toastLabel, attribute: .leading, relatedBy: .equal, toItem: toastContainer, attribute: .leading, multiplier: 1, constant: 5)
        let a2 = NSLayoutConstraint(item: toastLabel, attribute: .trailing, relatedBy: .equal, toItem: toastContainer, attribute: .trailing, multiplier: 1, constant: -5)
        let a3 = NSLayoutConstraint(item: toastLabel, attribute: .bottom, relatedBy: .equal, toItem: toastContainer, attribute: .bottom, multiplier: 1, constant: -15)
        let a4 = NSLayoutConstraint(item: toastLabel, attribute: .top, relatedBy: .equal, toItem: toastContainer, attribute: .top, multiplier: 1, constant: 15)
        toastContainer.addConstraints([a1, a2, a3, a4])
        
        let c1 = NSLayoutConstraint(item: toastContainer, attribute: .leading, relatedBy: .equal, toItem: controller.view, attribute: .leading, multiplier: 1, constant: 140)
        let c2 = NSLayoutConstraint(item: toastContainer, attribute: .trailing, relatedBy: .equal, toItem: controller.view, attribute: .trailing, multiplier: 1, constant: -140)
        let c3 = NSLayoutConstraint(item: toastContainer, attribute: .bottom, relatedBy: .equal, toItem: controller.view, attribute: .bottom, multiplier: 1, constant: -90)
        controller.view.addConstraints([c1, c2, c3])
        
        UIView.animate(withDuration: 0.0, delay: 0.0, options: .curveEaseIn, animations: {
            toastContainer.alpha = 1.0
        }, completion: { _ in
            UIView.animate(withDuration: 0.0, delay: delay, options: .curveEaseOut, animations: {
                toastContainer.alpha = 0.0
            }, completion: {_ in
                toastContainer.removeFromSuperview()
            })
        })
    }
    
    
}
