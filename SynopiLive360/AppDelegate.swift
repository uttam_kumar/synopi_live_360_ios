//
//  AppDelegate.swift
//  SynopiLive360
//
//  Created by Syncrhonous on 3/4/19.
//  Copyright © 2019 Syncrhonous. All rights reserved.
//

import AVFoundation
import HaishinKit
import Logboard
import UIKit
import Firebase
import FirebaseAuth
import UserNotifications

let logger = Logboard.with("com.haishinkit.Exsample.iOS")

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {
        
    let defaults = UserDefaults.standard
    var window: UIWindow?
    
    var lVController: LiveViewController?
    var alertComText: AlertCommonTextFieldViewController?
    var alertReco: AlertRecordingViewController?
    var alertSam: AlertSamplerateViewController?

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        // Override point for customization after application launch.
        
        let isVerified = defaults.string(forKey: "isVerifyied") ?? "no"

        let cVC = defaults.string(forKey: "currentViewController") ?? "StartingViewController"
        print("cVC: \(cVC)")
        
        if(cVC == "StartingViewController"){
            let mainStoryboard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
            let instantialViewC: StartingViewController = mainStoryboard.instantiateViewController(withIdentifier: "startingStoryBoard") as! StartingViewController
            
            self.window?.rootViewController = instantialViewC
            
            self.window?.makeKeyAndVisible()
        }else if(cVC == "MainViewController"){
            let mainStoryboard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
            let instantialViewC: MainViewController = mainStoryboard.instantiateViewController(withIdentifier: "mainStoryBoard") as! MainViewController
            
            self.window?.rootViewController = instantialViewC
            
            self.window?.makeKeyAndVisible()
        }
        
//        else if (cVC == "OtpCodeViewController"){
//            let mainStoryboard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
//            let instantialViewC: OtpCodeViewController = mainStoryboard.instantiateViewController(withIdentifier: "codeSentStoryBoard") as! OtpCodeViewController
//
//            self.window?.rootViewController = instantialViewC
//
//            self.window?.makeKeyAndVisible()
//        }
        
        else if (cVC == "LoggedInViewController" && isVerified == "yes"){
            let mainStoryboard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
            let instantialViewC: LoggedInViewController = mainStoryboard.instantiateViewController(withIdentifier: "serviceIdStoryBoard") as! LoggedInViewController
            
            self.window?.rootViewController = instantialViewC
            
            self.window?.makeKeyAndVisible()
        }else if (cVC == "LiveViewController"){
            let mainStoryboard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
            let instantialViewC: LiveViewController = mainStoryboard.instantiateViewController(withIdentifier: "cameraStoryBoard") as! LiveViewController
            
            self.window?.rootViewController = instantialViewC
            
            self.window?.makeKeyAndVisible()
        }else if (cVC == "StatusViewViewController"){
            let mainStoryboard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
            let instantialViewC: StatusViewViewController = mainStoryboard.instantiateViewController(withIdentifier: "statusStoryBoard") as! StatusViewViewController
            
            self.window?.rootViewController = instantialViewC
            
            self.window?.makeKeyAndVisible()
            
        }  else{
            let mainStoryboard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
            let instantialViewC: MainViewController = mainStoryboard.instantiateViewController(withIdentifier: "mainStoryBoard") as! MainViewController
            
            self.window?.rootViewController = instantialViewC
            
            self.window?.makeKeyAndVisible()
        }


        //phone verify part
        FirebaseApp.configure();
        if #available(iOS 10, *){
            UNUserNotificationCenter.current().requestAuthorization(options: [.badge, .sound, .alert], completionHandler: {(granted, error) in})
            application.registerForRemoteNotifications()
        }else{
            let notificationSettings = UIUserNotificationSettings(types: [.badge, .sound, .alert], categories: nil)
            UIApplication.shared.registerUserNotificationSettings(notificationSettings)
            UIApplication.shared.registerForRemoteNotifications()
        }
        
        
        lVController = self.window!.rootViewController as? LiveViewController
        alertComText = self.window!.rootViewController as? AlertCommonTextFieldViewController
        alertReco = self.window!.rootViewController as? AlertRecordingViewController
        alertSam = self.window!.rootViewController as? AlertSamplerateViewController
        
        let session = AVAudioSession.sharedInstance()
        do {
            try session.setPreferredSampleRate(44_100)
            // https://stackoverflow.com/questions/51010390/avaudiosession-setcategory-swift-4-2-ios-12-play-sound-on-silent
            if #available(iOS 10.0, *) {
                try session.setCategory(.playAndRecord, mode: .default, options: [.defaultToSpeaker, .allowBluetooth])
            } else {
                session.perform(NSSelectorFromString("setCategory:withOptions:error:"), with: AVAudioSession.Category.playAndRecord, with: [AVAudioSession.CategoryOptions.allowBluetooth])
                try? session.setMode(.default)
            }
            try session.setActive(true)
        } catch {
        }
        return true
    }

    func applicationWillResignActive(_ application: UIApplication) {
        print("applicationWillResignActive")
        alertComText?.alertView.viewScaleDismiss()
    }

    func applicationDidEnterBackground(_ application: UIApplication) {
        
        let cVC = defaults.string(forKey: "currentViewController") ?? "StartingViewController"
        print("cVC: \(cVC)")
        
        if(cVC == "LiveViewController"){
            lVController?.whenAppGoesBackground()
        }
    }

    func applicationWillEnterForeground(_ application: UIApplication) {
        let cVC = defaults.string(forKey: "currentViewController") ?? "StartingViewController"
        print("cVC: \(cVC)")
        if(cVC == "LiveViewController"){
            lVController!.whenAppGoesForeground()
            lVController!.internetIdentifier()
        }
    }

    func applicationDidBecomeActive(_ application: UIApplication) {
        print("app")
    }

    func applicationWillTerminate(_ application: UIApplication) {
        print("terminate")
    }


}

