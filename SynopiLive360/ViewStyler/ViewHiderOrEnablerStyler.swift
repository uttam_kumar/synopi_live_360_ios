//
//  ViewHider.swift
//  SynopiLive360
//
//  Created by Syncrhonous on 23/5/19.
//  Copyright © 2019 Syncrhonous. All rights reserved.
//

import Foundation

//view and button enabler/disabler and visibler/invisiler
class ViewHiderOrEnablerStyler: NSObject {
    
    func disOrEnableMultipleButtons(buttons: [UIButton], dissAble: [Bool]) {
        for (index, button) in buttons.enumerated() {
            switch dissAble[index] {
            case true:
                button.isEnabled = true
            case false:
                button.isEnabled = false
            }
            
        }
    }
    
    func hiderButtons(buttons: [UIButton], dissAble: [Bool]) {
        for (index, button) in buttons.enumerated() {
            switch dissAble[index] {
            case true:
                button.isHidden = true
            case false:
                button.isHidden = false
            }
            
        }
    }
    
    func viewCornerRadius(views: [UIView]) {
        for (view) in views.enumerated() {
            view.element.layer.cornerRadius = 8
            
        }
    }
    
    func buttonCornerRadius(views: [UIButton]) {
        for (view) in views.enumerated() {
            view.element.layer.cornerRadius = 8
            
        }
    }
    
}
