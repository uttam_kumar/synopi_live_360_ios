//
//  AnimatedLabel.swift
//  SynopiLive360
//
//  Created by Syncrhonous on 7/7/19.
//  Copyright © 2019 Syncrhonous. All rights reserved.
//


import Foundation
import UIKit
extension UILabel {

    func shakeLabel() {
        let shake = CABasicAnimation(keyPath: "position")
        shake.duration = 0.1
        shake.repeatCount = 2
        shake.autoreverses = true
        
        let fromPoint = CGPoint(x: center.x - 5, y: center.y)
        let fromValue = NSValue(cgPoint: fromPoint)
        
        let toPoint = CGPoint(x: center.x + 5, y: center.y)
        let toValue = NSValue(cgPoint: toPoint)
        
        shake.fromValue = fromValue
        shake.toValue = toValue
        
        layer.add(shake, forKey: "position")
    }
}

