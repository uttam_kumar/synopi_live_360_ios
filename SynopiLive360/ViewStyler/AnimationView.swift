//
//  ViewAnimation.swift
//  SynopiLive360
//
//  Created by Syncrhonous on 16/4/19.
//  Copyright © 2019 Syncrhonous. All rights reserved.
//


//visible and invisible views with animation on LiveViewController
import Foundation
import UIKit
extension UIView {

    func viewScaleDisplay() {
        let flash = CABasicAnimation(keyPath: "transform.scale")
        flash.duration = 0.5
        flash.fromValue = 0
        flash.toValue = 1.0
        flash.timingFunction = CAMediaTimingFunction(name: CAMediaTimingFunctionName.easeIn)
        print("hfsdh")
        layer.add(flash, forKey: nil)
    }
    
    
    func viewScaleDismiss() {
        let flash = CABasicAnimation(keyPath: "transform.scale")
        flash.duration = 0.55
        flash.fromValue = 1
        flash.toValue = 0.0
        flash.timingFunction = CAMediaTimingFunction(name: CAMediaTimingFunctionName.easeOut)
        layer.add(flash, forKey: nil)
    }
    
    
    func viewFlash() {
        let flash = CABasicAnimation(keyPath: "opacity")
        flash.duration = 0.5
        flash.fromValue = 1
        flash.toValue = 0.1
        flash.timingFunction = CAMediaTimingFunction(name: CAMediaTimingFunctionName.easeInEaseOut)
        flash.autoreverses = true
        flash.repeatCount = 1
        layer.add(flash, forKey: nil)
    }
    
}
