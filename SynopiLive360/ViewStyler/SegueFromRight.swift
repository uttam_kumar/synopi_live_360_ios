//
//  SegueFromRight.swift
//  SynopiLive360
//
//  Created by Syncrhonous on 13/5/19.
//  Copyright © 2019 Syncrhonous. All rights reserved.
//


//next page coming with slide animation from right
import Foundation
import UIKit
class SegueFromRight: UIStoryboardSegue {
    override func perform() {
        let src = self.source
        let dst = self.destination
        
        src.view.superview?.insertSubview(dst.view, aboveSubview: src.view)
        dst.view.transform = CGAffineTransform(translationX: src.view.frame.size.width, y: 0)
        //Users/syncmac/Documents/Synopi_Rtmp/SynopiLive360/SynopiLive360/Animation/SegueFromRight.swift
        UIView.animate(withDuration: 1, delay: 0.0, options: .curveEaseInOut, animations: {
            dst.view.transform = CGAffineTransform(translationX: 0, y: 0)}, completion: { finished in
                src.present(dst, animated: false, completion: nil)
            })
    }
}
