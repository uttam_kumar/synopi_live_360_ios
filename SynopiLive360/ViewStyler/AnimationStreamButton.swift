//
//  AnimationImage.swift
//  SynopiLive360
//
//  Created by Syncrhonous on 16/4/19.
//  Copyright © 2019 Syncrhonous. All rights reserved.
//


//stream button visible and invisible with animation
import Foundation
import UIKit
extension UIButton {
    
    func streamBtnDisplay() {
        let flash = CABasicAnimation(keyPath: "transform.scale")
        flash.duration = 0.5
        flash.fromValue = 0
        flash.toValue = 1.0
        flash.timingFunction = CAMediaTimingFunction(name: CAMediaTimingFunctionName.easeIn)
        layer.add(flash, forKey: nil)
    }
    
    
    func streamBtnDismiss() {
        let flash = CABasicAnimation(keyPath: "transform.scale")
        flash.duration = 0.55
        flash.fromValue = 1
        flash.toValue = 0.0
        flash.timingFunction = CAMediaTimingFunction(name: CAMediaTimingFunctionName.easeOut)
        layer.add(flash, forKey: nil)
    }
    
}
