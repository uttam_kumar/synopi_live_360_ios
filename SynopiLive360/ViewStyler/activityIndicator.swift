//
//  ExtensionFile.swift
//  SynopiLive360
//
//  Created by Syncrhonous on 12/5/19.
//  Copyright © 2019 Syncrhonous. All rights reserved.
//


//used for spiner/ activity indicator
import Foundation
var vSpinner : UIView?
var backGroundView : UIView?
var whiteView : UIView?
var loadingLabel : UILabel?

extension UIViewController {
    func showSpinner(onView : UIView, msg :String) {
        
        backGroundView = UIView(frame: CGRect(x: 0, y: 0, width: onView.frame.size.width, height: onView.frame.size.height))
        backGroundView!.backgroundColor = UIColor.black.withAlphaComponent(0.6)
        self.view.addSubview(backGroundView!)
        
        whiteView = UIView(frame: CGRect(x: backGroundView!.frame.size.width / 2 - 75, y: backGroundView!.frame.size.height / 2 - 50, width: 150, height: 100))
        whiteView!.backgroundColor = #colorLiteral(red: 0.937254902, green: 0.937254902, blue: 0.9568627451, alpha: 1)
        whiteView?.layer.cornerRadius = 8
        backGroundView?.addSubview(whiteView!)
        
        
        loadingLabel = UILabel(frame: CGRect(x: 0 , y: whiteView!.frame.size.width/2, width: whiteView!.frame.size.width, height: 20))
        //To set the color
        //loadingLabel!.backgroundColor = UIColor.white
        loadingLabel!.textColor = #colorLiteral(red: 0.6799238037, green: 0.3529885309, blue: 0.7658930421, alpha: 1)
        //To set the font Dynamic
        //loadingLabel!.font = UIFont(name: "Helvetica-Regular", size: 20.0)
        //To set the system font
        //loadingLabel!.font = UIFont.systemFont(ofSize: 20.0)
        loadingLabel!.textAlignment = .center
        loadingLabel!.text = msg
        whiteView!.addSubview(loadingLabel!)
        
        
        let spinnerView = UIView.init(frame: whiteView!.bounds)
        //spinnerView.backgroundColor = UIColor.init(red: 0.5, green: 0.5, blue: 0.5, alpha: 0.6)
        let ai = UIActivityIndicatorView.init(style: .whiteLarge)
        ai.color = #colorLiteral(red: 0.6799238037, green: 0.3529885309, blue: 0.7658930421, alpha: 1)
        ai.startAnimating()
        ai.center = spinnerView.center
        
        DispatchQueue.main.async {
            spinnerView.addSubview(ai)
            whiteView!.addSubview(spinnerView)
        }
        
        vSpinner = spinnerView
    }
    
    func removeSpinner() {
        DispatchQueue.main.async {
            vSpinner?.removeFromSuperview()
            whiteView?.removeFromSuperview()
            backGroundView?.removeFromSuperview()
            vSpinner = nil
        }
    }
    

    func grayColor(onView : UIView) {
        backGroundView = UIView(frame: CGRect(x: 0, y: 0, width: onView.frame.size.width, height: onView.frame.size.height))
        backGroundView!.backgroundColor = UIColor.black.withAlphaComponent(0.6)
        self.view.addSubview(backGroundView!)
    }


}

