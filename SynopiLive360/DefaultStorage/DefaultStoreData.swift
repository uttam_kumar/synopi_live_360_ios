//
//  DefaultStoreData.swift
//  SynopiLive360
//
//  Created by Syncrhonous on 23/5/19.
//  Copyright © 2019 Syncrhonous. All rights reserved.
//

import Foundation

class DefaultStoreData: NSObject {
    let defaults = UserDefaults.standard
    func dataStoreOnDefaultStorage(datas: [String], keys: [String]){
        for (index, data) in datas.enumerated() {
            print("DefaultStoreData: \(data), keys: \(keys[index])")
            defaults.set(data, forKey:keys[index])

        }
    }

}
