//
//  UserPermission.swift
//  SynopiLive360
//
//  Created by Syncrhonous on 21/5/19.
//  Copyright © 2019 Syncrhonous. All rights reserved.
//


//all user permission controlled here
import Foundation
import UIKit
import Photos

class UserPermission: NSObject {
    
    //phone settings display
    func presentPhoneSettings(viewController: UIViewController, per: String) {
        print(per)
        
        let alertController = UIAlertController(title: "App Permission Required", message: "\(per) highly required for Synopi Live 360.", preferredStyle: .alert)
        alertController.addAction(UIAlertAction(title: "Cancel", style: .default))
        alertController.addAction(UIAlertAction(title: "Settings", style: .cancel) { _ in
            if let url = URL(string: UIApplication.openSettingsURLString) {
                if #available(iOS 10.0, *) {
                    UIApplication.shared.open(url, options: [:], completionHandler: { _ in
                        // Handle
                    })
                } else {
                    // Fallback on earlier versions
                }
            }
        })
        viewController.present(alertController, animated: true)
    }
    
 
    func allPermissionChecker() -> String {
        var givenPermission: String = ""
        
        if checkCameraAccess() && checkMicrophoneAccess() && checkPhotoLibraryPermission(){
            givenPermission = ""
        }else if checkCameraAccess() && !checkMicrophoneAccess() && checkPhotoLibraryPermission(){
            givenPermission = "Microphone permission is"
        }else if !checkCameraAccess() && checkMicrophoneAccess() && checkPhotoLibraryPermission(){
            givenPermission = "Camera permission is"
        }else if checkCameraAccess() && checkMicrophoneAccess() && !checkPhotoLibraryPermission(){
            givenPermission = "Photo Library permission is"
        }else if !checkCameraAccess() && !checkMicrophoneAccess() && checkPhotoLibraryPermission(){
            givenPermission = "Camera and Microphone permissions are"
        }else if checkCameraAccess() && !checkMicrophoneAccess() && !checkPhotoLibraryPermission(){
            givenPermission = "Microphone and Photos Library permissions are"
        }else if !checkCameraAccess() && checkMicrophoneAccess() && !checkPhotoLibraryPermission(){
            givenPermission = "Camera and Photo Library permissions are"
        }else if !checkCameraAccess() && !checkMicrophoneAccess() && !checkPhotoLibraryPermission(){
            givenPermission = "Camera, Microphone and Photo Library permissions are"
        }
        
        return givenPermission
    }
    
    func micAndCameraPermissionChecker() -> String {
        var givenPermission: String = ""
        if checkCameraAccess() && checkMicrophoneAccess(){
            givenPermission = ""
        }else if checkCameraAccess() && !checkMicrophoneAccess(){
            givenPermission = "Microphone permission is"
        }else if !checkCameraAccess() && !checkMicrophoneAccess(){
            givenPermission = "Camera and Microphone permissions are"
        }else if !checkCameraAccess() && checkMicrophoneAccess(){
            givenPermission = "Microphone permission is"
        }
        return givenPermission
    }
    
    //check camera and microphone is granted
    func checkAllPermissionGranted() -> Bool{
        
        print("photo permission: \(checkPhotoLibraryPermission())")
        if(checkCameraAccess() && checkMicrophoneAccess() && checkPhotoLibraryPermission()){
            return true
        }
        return false
        
    }
    
    //check camera and microphone is granted
    func checkMicAndCameraPermissionGranted() -> Bool{
        
        if(checkCameraAccess() && checkMicrophoneAccess()){
            return true
        }
        return false
    }
    
    
    //    //storage access permission
    func checkPhotoLibraryPermission() -> Bool {
        var isPermited: Bool = false
        let status = PHPhotoLibrary.authorizationStatus()
        switch status {
        case .authorized:
            //handle authorized status
            isPermited = true
            break
        case .denied, .restricted :
            //handle denied status
            isPermited = false
            break
        case .notDetermined:
            // ask for permissions
            PHPhotoLibrary.requestAuthorization { status in
                switch status {
                case .authorized:
                    // as above
                    isPermited = true
                    break
                case .denied, .restricted:
                    // as above
                    isPermited = false
                    break
                case .notDetermined:
                    // won't happen but still
                    isPermited = false
                    break
                @unknown default:
                    print("error")
                }
            }
        @unknown default:
            print("error")
        }
        
        return isPermited
    }
    
    //microphone access permission
    func checkMicrophoneAccess() -> Bool {
        var  isparmited = false
        
        switch AVAudioSession.sharedInstance().recordPermission  {
        case .denied:
            print("Denied, request permission from settings")
        //presentPhoneSettings()
        case .granted:
            print("Authorized, proceed")
            isparmited = true
        case .undetermined:
            AVAudioSession.sharedInstance().requestRecordPermission { success in
                if success {
                    print("Permission granted, proceed")
                    isparmited = true
                } else {
                    print("Permission denied")
                    isparmited = false
                }
            }
        @unknown default:
            print("error")
        }
        return isparmited
    }
    
    
    //camera access permission
    func checkCameraAccess() -> Bool {
        var  isparmited = false
        switch AVCaptureDevice.authorizationStatus(for: .video) {
        case .denied:
            print("Denied, request permission from settings")
        //presentPhoneSettings()
        case .restricted:
            print("Restricted, device owner must approve")
            isparmited = false
        case .authorized:
            print("Authorized, proceed")
            isparmited = true
        case .notDetermined:
            AVCaptureDevice.requestAccess(for: .video) { success in
                if success {
                    print("Permission granted, proceed")
                    isparmited = true
                } else {
                    print("Permission denied")
                    isparmited = false
                }
            }
        @unknown default:
            print("error")
        }
        return isparmited
    }
 
}


